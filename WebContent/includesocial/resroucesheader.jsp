<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div id="header" class="header-logo-app header-dark"
	style="transition: all 350ms ease; background-image: linear-gradient(to left, black, #e65c00, #ffc299)">
	<a href="#" class="header-title back-button">FARMMER MOBILE APP</a> <a
		href="#" class="header-logo disabled"></a> <a href="#"
		class="header-icon header-icon-1 back-button font-10 no-border"><i
		class="fa fa-chevron-left"></i></a> <a
		href="${pageContext.request.contextPath}/member/news/PF2"
		class="header-icon header-icon-2 font-14"><i class="fas fa-home"></i></a>
	<a href="#"
		class="header-icon header-icon-4 hamburger-animated no-border"
		data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em
		class="hm3"></em></a>
</div>