<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<jsp:include page="/include/resources_js.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>แบบสอบถาม</title>
</head>
<style>
.content {
	margin: 0px 16px 30px 16px;
	margin-top: -58px
}

* {
	box-sizing: border-box
}
/* Set height of body and the document to 100% */
body, html {
	height: 100%;
	margin: 0;
	font-family: Arial;
}

/* Style tab links */
.tablink {
	background-color: #555;
	color: white;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	font-size: 17px;
}

.card {
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
	transition: 0.3s;
	width: 100%;
}

.tablink:hover {
	background-color: #777;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
	color: white;
	display: none;
	padding: 50px 10px;
	height: 100%;
	margin-top: 60px
}

#Bottom1 {
	background-color: #fff3e6;
}

#Bottom2 {
	background-color: #d9f2d9;
}
</style>
<body>
	<jsp:include page="/include/resources_preloader.jsp"></jsp:include>

	<div id="page-transitions">
		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		<div id="page-content" class="page-content">
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!-- start -->

					<div align="center">
						<h1>แบบสอบถาม</h1>
						<button class="tablink"
							onclick="openPage('Bottom1', this, '#cc9966')">ตอบแล้ว</button>
						<button class="tablink"
							onclick="openPage('Bottom2', this, '#39ac39')" id="defaultOpen">ยังไม่ตอบ</button>


					</div>
					<div class="content demo-buttons" align="center">
						<div id="Bottom1" class="tabcontent card">
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 1 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer2">กดที่นี้</a>
							</p>
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 2 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer2">กดที่นี้</a>
							</p>
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 3 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer2">กดที่นี้</a>
							</p>
						</div>

						<div id="Bottom2" class="tabcontent card">
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 1 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer">กดที่นี้</a>
							</p>
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 2 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer">กดที่นี้</a>
							</p>
							<p style="font-family: verdana; font-size: 16px;">
								แบบสอบถามชุดที่ 3 <a
									href="${pageContext.request.contextPath}/member/questionnaire/Answer">กดที่นี้</a>
							</p>


						</div>
					</div>
					<div align="center" style="margin-top: 10px">
						<a href="#"
							class="button button-round button-dark-3d button-dark back-button"
							style="font-family: verdana; font-size: 16px;">ย้อนกลับ</a> <a
							href="${pageContext.request.contextPath}/"
							class="button button-round button-dark-3d button-dark"
							style="font-family: verdana; font-size: 16px;"><i
							class="fa fa-home color-blue-dark"></i> หน้าหลัก</a>
					</div>

					<!-- end -->
				</div>
			</div>
		</div>
		<jsp:include page="/include/resources_notify.jsp"></jsp:include>
		<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
		<a href="#" class="back-to-top-badge back-to-top-small"><i
			class="fa fa-angle-up"></i>Back to Top</a>
	</div>
	<script>
		function openPage(pageName, elmnt, color) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablink");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].style.backgroundColor = "";
			}
			document.getElementById(pageName).style.display = "block";
			elmnt.style.backgroundColor = color;
		}

		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
	</script>
</body>