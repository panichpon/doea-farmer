<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<jsp:include page="/include/resources_js.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>แบบสอบถาม</title>
</head>

<body>
	<jsp:include page="/include/resources_preloader.jsp"></jsp:include>

	<div id="page-transitions">
		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		<div id="page-content" class="page-content">
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!-- start -->
					<div align="center">
						<div class="container">
							<br>
							<div class="blockquote-2 content" style="background-image: linear-gradient(to bottom right, #ffb3d1, #ff4d94); padding-bottom: 17px; border-radius: 25px;">
								<h1 style="text-align: center; padding-top: 25px; font-size: 24px; color: white;">
								แบบสอบถาม	</h1>
							</div>
							<div style="border:2px solid #ff1a75; padding: 10px; background-color: #ffe6f0" >
							<h4 >แบบสอบถาม</h4>
							<br>
							<h4>	วันที่กรอกแบบสอบถาม <br>	<input  style="background-color: #ffe6f0; text-align: center; "type="date" id="start" name="trip-start">
							</h4>
							<br>
							<div align="left">
								<form action="/action_page.php">
									<h6>ข้อ 1 _______________________</h6>
									<div class="form-check">
										<label class="form-check-label" for="radio1"> <input
											type="radio" class="form-check-input" id="radio1"
											name="optradio" value="option1" checked>ก.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio2"> <input
											type="radio" class="form-check-input" id="radio2"
											name="optradio" value="option2">ข.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio3"> <input
											type="radio" class="form-check-input" id="radio3"
											name="optradio" value="option3">ค.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio4"> <input
											type="radio" class="form-check-input" id="radio4"
											name="optradio" value="option4">ง.
										</label>
									</div>
									<br>
								</form>
								<form action="/action_page.php">
									<h6>ข้อ 2 _______________________</h6>
									<div class="form-check">
										<label class="form-check-label" for="radio1"> <input
											type="radio" class="form-check-input" id="radio1"
											name="optradio" value="option1" checked>ก.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio2"> <input
											type="radio" class="form-check-input" id="radio2"
											name="optradio" value="option2">ข.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio2"> <input
											type="radio" class="form-check-input" id="radio2"
											name="optradio" value="option2">ค.
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label" for="radio2"> <input
											type="radio" class="form-check-input" id="radio2"
											name="optradio" value="option2">ง.
										</label>
									</div>
								</form>
							</div>
							<br>
							
							
							<button type="submit"
								class="button button-round button-pink-3d button-pink"
								data-toggle="modal" data-target="#done">ส่งคำตอบ</button>
							<div class="modal" id="done" style="margin-top: 230px">
								<div class="modal-dialog" style="padding: 15px 20px; background-color: #ffe6f0;">
									<div class="modal-content">
									
							</div>
										<!-- Modal Header -->
										<div class="modal-header" align="center">
											<h6 class="modal-title text-center"  ">ยืนยันการส่งคำตอบแบบสอบถามนี้</h6>
										</div>

										<!-- Modal body -->
										<div class="modal-body">
											<a
												href="${pageContext.request.contextPath}/member/questionnaire/home"
												class="button button-rounded button-green">ยืนยัน</a>
											<button type="button" class="btn btn-danger"
												>ยกเลิก</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- end -->
				</div>
			</div>
		</div>
		<jsp:include page="/include/resources_notify.jsp"></jsp:include>
		<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
		<a href="#" class="back-to-top-badge back-to-top-small"><i
			class="fa fa-angle-up"></i>Back to Top</a>
	</div>
</body>
</html>