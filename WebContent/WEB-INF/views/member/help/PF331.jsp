<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>โครงการช่วยเหลือ กรมส่งเสริมการเกษตร</title>
</head>
<body>
	<jsp:include page="/includeFromNeed/resources_headerTeamE.jsp"></jsp:include>
	<div class="decoration decoration-margins"></div>

	<div class="card " style="width: 100%; background-color: #c7c5c4;">
		<table class="table">
			<tbody>
				<tr>
					<td class="botton button-red-3d"
						style="background-color: #F2F2F0; width: 50%" align="center"><h6>ปีที่ผ่านมา</h6>

					</td>
					<td style="background-color: #F2F2F0; width: 50%" align="center"><a
						href="${pageContext.request.contextPath}/member/help/PF33"><h6
								style="color:black;">ปัจจุบัน</h6></a></td>
				</tr>
			</tbody>
		</table>
		<div align="center" style="margin: auto 10px 10px 10px">
			<form action="/action_page.php">
				<select name="cars">
					<option value="year">ปี</option>
					<option value="2019" id="2019">2019</option>

					<option value="2018">2018</option>
					<option value="2017">2017</option>
				</select>
			</form>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="container" class="card " align="center"
					style="width: 100%; margin: auto auto auto auto">
					<div class="card-header" style="background-color: #a1253e;">
						<div class="media">
							<i class="far fa-check-square fa-2x" style="color: white"></i>
							<div class="media-body">
								<h4 style="color: white;">โครงการที่เข้าร่วม</h4>
							</div>
						</div>
					</div>
					<div class="card-body" style="background-color: white;">
						<div class="media">
						<i class="fas fa-check" style="color: #D35770"></i>
							<div class="media-body">
								<a href="#" style="color: black;">-.ครม.อนุมัติขยายระยะเวลาการดำเนินงานโครงการตามมติคณะรัฐมนตรี
									จำนวน 4 โครงการ</a><br>
								<div class="media">
									<div class="media-body">
										<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="media">
							<i class="fas fa-check" style="color: #D35770"></i>
							<div class="media-body">
								<a href="#" style="color: black;">-.การขอขยายระยะเวลาการดำเนินงานโครงการตามมติคณะรัฐมนตรี
									จำนวน 4 โครงการ จากวันที่ 8 มิถุนายน 2562 ต่อเนื่องถึงวันที่ 7
									มิถุนายน 2563</a><br>
								<div class="media">
									<div class="media-body">
										<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
										
									</div>
								</div>
							</div>
						</div>
						<div id="demo1" class="collapse">
							<hr>
							<div class="media">
								<i class="fas fa-check" style="color: #D35770"></i>
								<div class="media-body">
									<a href="#" style="color: black;">-.ธ.ก.ส.พร้อมรับนโยบายรัฐบาล</a><br>
									<div class="media">
										<div class="media-body">
											<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
												
										</div>
									</div>
									<hr>

								</div>
							</div>
							<div class="media">
								<i class="fas fa-check" style="color: #D35770"></i>
								<div class="media-body">

									<a href="#" style="color: black;">-.กรมประมงขันนอตจนท.เตรียมรับอุทกภัย</a><br>
									<div class="media">
										<div class="media-body">
											<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
												
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<button type="button" class="btn"  style="background-color: #a1253e;color: white;"
							data-toggle="collapse" data-target="#demo1">ย่อ/ขยาย</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="container" class="card bg-light" align="center"
					style="width: 100%; margin: 10px 10% auto auto">
					<div class="card-header" style="background-color: #a1253e;">
						<div class="media">
							<i class="fas fa-user-check fa-2x" style="color: white;"></i>
							<div class="media-body">
								<h4 style="color: white;">โครงการที่ถูกลิสต์รายชื่อ</h4>
							</div>
						</div>
					</div>
					<div class="card-body" style="background-color: white;">
						<div class="media">
							<i class="fas fa-star " style="color: #FF4646"></i>
							<div class="media-body" >
								<a href="#" style="color: black;">-.ครม.อนุมัติขยายระยะเวลาการดำเนินงานโครงการตามมติคณะรัฐมนตรี
									จำนวน 4 โครงการ</a><br>
								<div class="media">
									<div class="media-body">
										<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
											
									</div>
								</div>
							</div>
						</div>
						<div id="demo2" class="collapse">
							<hr>
							<div class="media">
								<i class="fas fa-star " style="color: #FF4646"></i>
								<div class="media-body">
									<a href="#" style="color: black;">-.กรมประมงขันนอตจนท.เตรียมรับอุทกภัย</a><br>
									<div class="media">
										<div class="media-body">
											<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
												
										</div>
									</div>

								</div>
							</div>
						</div>
						<hr>
						<button type="button" class="btn " style="background-color: #a1253e;color: white;"
							data-toggle="collapse" data-target="#demo2">ย่อ/ขยาย</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="container" class="card bg-light" align="center"
					style="width: 100%; margin: 10px 10% 10% auto">
					<div class="card-header" style="background-color: #a1253e;">
						<div class="media">
							<i class="far fa-times-circle fa-2x" style="color: white;"></i>
							<div class="media-body">
								<h4 style="color: white;">โครงการที่ไม่ถูกลิสต์รายชื่อ</h4>

							</div>
						</div>
					</div>
					<div class="card-body" style="background-color: white;">
						<div class="media">
							<i class="fas fa-minus-circle" style="color: #FF0000"></i>
							<div class="media-body">
								<a href="#" style="color: black;">-.ครม.รับทราบราคายาง-ราคาปาล์ม
									ขยับสูงขึ้นจากนโยบายของภาครัฐ</a><br>
								<div class="media">
									<div class="media-body">
										<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
											
									</div>
								</div>
							</div>
						</div>

						<div id="demo3" class="collapse">
							<hr>
							<div class="media">
								<i class="fas fa-minus-circle" style="color: #FF0000"></i>
								<div class="media-body">
									<a href="#" style="color: black;">-.มกอช. และกรมส่งเสริมการเกษตร
										เยี่ยมกลุ่มเกษตกร มะม่วงรักบ้านเกิด</a><br>
									<div class="media">
										<div class="media-body">
											<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
												
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div class="media">
								<i class="fas fa-minus-circle" style="color: #FF0000"></i>
								<div class="media-body">

									<a href="#" style="color: black;">-.กฟก. เผยรายชื่อผู้แทนเกษตรกรชุดใหม่
										ที่ชนะผลเลือกตั้ง 16 มิ.ย.62</a><br>
									<div class="media">
										<div class="media-body">
											<i class="fas fa-calendar-alt"></i> 21/เมษายน/2562 
												
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<button type="button" class="btn " style="background-color: #a1253e;color: white;"
							data-toggle="collapse" data-target="#demo3">ย่อ/ขยาย</button>

					</div>
				</div>
			</div>
		</div>
		<hr style="margin: 50px 10px 10px 10px">
		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
	</div>


	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>