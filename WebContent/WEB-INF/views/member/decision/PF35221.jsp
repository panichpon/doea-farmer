<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title> วิเคราะห์ด้านการตลาด - จัดการพืชที่สนใจ </title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
</head>
<body>
  
	<div id="page-transitions dark">
		<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF3522"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
		</div>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		
			<div class="container content" style="margin-top: 80px;">
				<div class="blockquote-2 "
					style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
					<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-combo-chart-100.png">
					<h4 class="small-button"
						style="text-align: center; padding-top: 25px; padding-left: 20px;">
						วิเคราะห์ด้านการตลาด	</h4>
				</div>
			</div>
		
		<div class="content content-boxed content-boxed-padding" style="border: 2px solid #FF8E8E;">
				<div class="above-overlay">
					<table style="background: none; border: none;">
						<tr style="background: none; border: none;">
							<td style="background: none; border: none;">
								<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-cauliflower-100.png" 
								style="width: 50px; background-color: #FF9E9E; padding: 5px; border-radius: 50px;">
							</td>
							<td style="background: none; border: none; text-align: left;">
								<h5 class="uppercase ultrabold small-top">	จัดการพืชที่สนใจ	</h5>
							</td>
						</tr>
					</table>
					<div class="decoration full-top"></div>
					
						<table style="background: none; border: none;">
							<tr style="background: none; padding: 0px; border: none;">
								<td style="background: none; padding: 0px; border: none; font-size: 18px;">	ประเภท	</td>
							
								<td style="background: none; padding: 0px; border: none;">
									<div class="select-box select-box-2 half-bottom">
										<select style="background-image: linear-gradient(to bottom right, #FFCAD4, #F3ACB6);">
											<option value="volvo">	พืชไร่	</option>
											<option value="saab">	พืชสวน	</option>
											<option value="mercedes">	ผลไม้	</option>
											<option value="audi">	อื่นๆ		</option>
										</select>
									</div>
								</td>
							</tr>
							<tr style="background: none; padding: 0px; border: none;">
								<td style="background: none; padding: 0px; border: none; font-size: 18px;">	พืช	</td>
							
								<td style="background: none; padding: 0px; border: none;">
									<div class="select-box select-box-2 half-bottom">
										<select style="background-image: linear-gradient(to bottom right, #FFCAD4, #F3ACB6);">
											<option value="volvo">	ข้าวโพด	</option>
											<option value="saab">	ข้าวสาร	</option>
											<option value="mercedes">	มันสำปะหลัง	</option>
											<option value="audi">	อ้อย	</option>
											<option value="audi">	อื่นๆ		</option>
										</select>
									</div>
								</td>
							</tr>
						</table>
						
						<table style="background: none; border: none;">
							<tr style="background: none; border: none;">
								<td style="background: none; border: none; font-size: 18px; text-align: left; padding-left: 40px; width: 40%;">
									<strong>	ประเภท :	</strong>
								</td>
								<td style="background: none; border: none; font-size: 18px; text-align: left; width: 60%;">
									พืชไร่
								</td>
							</tr>
							<tr style="background: none; padding: 0px; border: none;">
								<td style="background: none; border: none; font-size: 18px; text-align: left; padding-left: 40px; width: 40%;">
									<strong>	พืช :	</strong>
								</td>
								<td style="background: none; border: none; font-size: 18px; text-align: left; width: 60%;">
									ข้าวโพด
								</td>
							</tr>
						</table>
						
					
					
					<div class="content" style="padding-top: 30px; padding-left: 0px; padding-right: 0px;">
						<a data-deploy-menu="notification-modal-3" href="#" class="button button-round button-pink-3d button-pink" style=" font-size: 18px; background-color: #890016; width: 95%; text-align: center;">
							เพิ่ม
						</a>
					</div>
				</div>
				
			</div>	
			
			<div id="notification-modal-3" data-menu-size="345" class="menu-wrapper menu-light menu-modal" style="text-align: center;">
				<div style="padding: 20px; text-align: center;">
					<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-check-file-100.png" style="text-align: center; width: 40%; margin-left: 70px;">
					<h4 style="margin-top: 20px;">	ยืนยันการเพิ่มพืชที่สนใจ?	</h4>
						<table style="background: none; border: none; margin-top: 50px;">
							<tr style="background: none; padding: 0px; border: none;">
								<td style="color: white; background: none; padding: 0px; border: none; font-size: 18px; width: 50%;">
									<a href="${pageContext.request.contextPath}/member/decision/PF3522" class="button button-round button-pink-3d button-pink" style=" font-size: 18px; background-color: #890016; width: 95%; text-align: center;">
										ยืนยัน
									</a>
								</td>
								
								<td style="background: none; padding: 0px; border: none; font-size: 18px; width: 50%;">
									<a href="${pageContext.request.contextPath}/member/decision/PF35221" class="button button-round button-pink-3d button-pink" style=" font-size: 18px; background-color: #660011; width: 95%; text-align: center;">
										ยกเลิก
									</a>
								</td>
							</tr>
						</table>
				</div>
			</div>
			
		
		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
	</div>

	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>