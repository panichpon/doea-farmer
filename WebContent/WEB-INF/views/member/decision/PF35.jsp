<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>ระบบสนับสนุนการตัดสินใจ</title>
</head>

<body>

	
<div id="page-transitions">
		<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
		<!--
			<a href="${pageContext.request.contextPath}/member/decision/PF352"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
		-->
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
		</div>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
<!--  
	<jsp:include page="/include/resources_header.jsp"></jsp:include>
	<jsp:include page="/include/resources_menu.jsp"></jsp:include>
-->
		
			<div id="page-transitions">		
				<div id="page-content" class="page-content">	
					<div id="page-content-scroll"><!--Enables this element to be scrolled --> 
						<div class="blockquote-2 content" style="background-image: linear-gradient(to bottom right, #F97794, #623AA2); padding-bottom: 17px; border-radius: 50px;margin-bottom: 20px">
							<img alt="" src="/DOEA-FARMER/contents/images/icons8-idea-100.png">
							<h4 style="text-align: center; padding-top: 25px; padding-left: 50px;font-size: 23px;color: white;">
								ระบบสนับสนุนการตัดสินใจ</h4>
						</div>
						<!-- 
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;background-color: #AD1448;">
							<div class="homepage-cta" >
								
								<h4 class="color-white"><i class="fa fa-lightbulb"></i>  ระบบสนับสนุนการตัดสินใจ </h4>
								
							</div>
						</div>
						-->
						
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;">
						 	
							<div class="homepage-cta" >
								<!--  
								<div class="page-login half-bottom">
								
								<div class="content" style="border: 2px solid #3F031A;">
								-->
									<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #F095FF, #F64848); padding-bottom: 17px; border-radius: 50px;margin-bottom: 10px">
										<img alt="" src="/DOEA-FARMER/contents/images/icons8-sales-performance-100.png" style="width: 60px;height: 80%;padding-left: 5px">
										<h3 style="text-align: center; padding-top: 15px; padding-left: 20px; color: white;margin-bottom: 0px">ประสิทธิภาพ</h3>
									</div>
									
									
									<div class="container content" style="margin-top: 0px;margin-left: 15px;margin-bottom: 10px">
										<a href="${pageContext.request.contextPath}/member/decision/PF351">
										<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;height: 60px">
											<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-checked-100-2.png" style="width: 55px;height: 90%">
											<h5 style="text-align: center; padding-top: 20px; padding-left: 20px; color: black;">วิเคราะห์ทางเลือก	</h5>
										</div>
										</a>
									</div>
									
									<div class="container content" style="margin-top: 0px;margin-left: 15px;margin-bottom: 10px">
										<a href="${pageContext.request.contextPath}/member/decision/PF352">
										<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;height: 60px">
											<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-combo-chart-100.png" style="width: 55px;height: 90%">
											<h5 style="text-align: center; padding-top: 20px; padding-left: 20px; color: black;">วิเคราะห์ด้านการตลาด	</h5>
										</div>
										</a>
									</div>
									
									<div class="container content" style="margin-top: 0px;margin-left: 15px;margin-bottom: 10px">
										<a href="${pageContext.request.contextPath}/member/decision/PF353">
										<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;height: 60px">
											<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-truck-100.png" style="width: 55px;height: 90%">
											<h5 style="text-align: center; padding-top: 20px; padding-left: 20px; color: black;">วิเคราะห์โลจิสติกส์	</h5>
										</div>
										</a>
									</div>
								
									
									<div class="decoration opacity-50"></div>
									
									<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #F095FF, #F64848); padding-bottom: 17px; border-radius: 50px;margin-bottom: 10px">
										<a href="${pageContext.request.contextPath}/member/decision/PF354">
										<img alt="" src="/DOEA-FARMER/contents/images/icons8-scales-100.png" style="width: 60px;height: 100%">
										<h4 style="text-align: center; padding-top: 15px; padding-left: 20px; color: white;margin-bottom: 0px">ความยุติธรรม(กลุ่ม)</h4>
										</a>										
									</div>
									
									<div class="blockquote-2 " style="background-image: linear-gradient(to bottom right, #F095FF, #F64848); padding-bottom: 17px; border-radius: 50px;margin-bottom: 10px">
										<a href="${pageContext.request.contextPath}/member/decision/PF355">
										<img alt="" src="/DOEA-FARMER/contents/images/icons8-scales-100.png" style="width: 60px;height: 100%">
										<h4 style="text-align: center; padding-top: 15px; padding-left: 20px; color: white;margin-bottom: 0px">ความเสถียร(กลุ่ม)</h4>
										</a>										
									</div>
									
									
								<!-- 	
								</div>
								 -->
							</div>
							
						</div>
							
					
					</div>
				</div>
			</div>	
			
			<jsp:include page="/include/resources_footer.jsp"></jsp:include>
			
		
	
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>