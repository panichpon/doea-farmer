<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Manage Point Earnings</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<style>
.tab {
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

.tab button {
	background-color: inherit;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	transition: 0.3s;
}

.tab button:hover {
	background-color: #ddd;
}

.tab button.active {
	background-color: #ccc;
}

.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}
</style>
</head>
<body>
	<div id="header" class="header-logo-app header-dark"
		style="background-image: linear-gradient(to right, black, #AA0846);">
		<a href="${pageContext.request.contextPath}/member/decision/PF352"
			class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title back-button"><strong
			style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> <a
			href="${pageContext.request.contextPath}/member/news/PF2"
			class="header-icon header-icon-2" style="font-size: 20px;"><i
			class="fa fa-home font-24"></i></a> <a href="#"
			class="header-icon header-icon-3"><i class="fa fa-d"
			style="color: black;"></i></a> <a href="#"
			class="header-icon header-icon-4 hamburger-animated no-border"
			data-deploy-menu="menu-1" style="font-size: 20px;"></a>
	</div>
	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>

	<div class="page-content header-clear-medium" style="padding-top: 50px">
		<div class="container content" style="margin-top: 20px">
			<div class="blockquote-2 "
				style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
				<img alt=""
					src="/DOEA-FARMER/contents/images/icons8-combo-chart-100.png">
				<h4
					style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">
					วิเคราะห์ด้านการตลาด</h4>
			</div>
		</div>
		<div class="content border border-danger">
			<form style="margin: 10px">
				<div class="form-group">
					<select class="form-control" id="exampleFormControlSelect1">
						<option selected>ประเภท</option>
						<option>1</option>
						<option>2</option>
					</select>
				</div>
				<div class="form-group">
					<select class="form-control" id="exampleFormControlSelect1">
						<option selected>พืช</option>
						<option>1</option>
						<option>2</option>

					</select>
				</div>
				<div class="form-group">
					<select class="form-control" id="exampleFormControlSelect1">
						<option selected>รัชมี(ก.ม.)</option>
						<option>1</option>
						<option>2</option>
					</select>
				</div>
			</form>
		</div>
		<div class="content" style="margin-bottom: 10px">
			<div class="tabs">
				<div class="tab-pill-titles"
					data-active-tab-background="bg-green-dark">
					<a href="#"
						data-tab-pill="tab-2" style="width: 50%; background-color:#D191EA"> ดูแผนที่ </a> 
					<a href="#" data-tab-pill="tab-1"
						style="width: 50%; background-color:#F2ABAB">
						ดูรายชื่อ </a>
				</div>
				<div class="tab-pill-content" >
					<div class="tab-item active-tab" id="tab-2" style="padding: 10px;background-color:#D191EA;margin-top: -20px">
						<div id="googleMapID"
							style="width: 100% !important; height: 350px !important;"></div>
						<script>
							function myMap() {
								var myLatLng = {
									lat : 16.2442727,
									lng : 103.2548162
								};
								var maps = new google.maps.Map(document
										.getElementById('googleMapID'), {
									zoom : 15,
									center : myLatLng,
									mapTypeId : google.maps.MapTypeId.HYBRID
								});
								var locations = [
										[ 'วัดลาดปลาเค้า', 16.2442727,
												103.2511102 ],
										[ 'หมู่บ้านอารียา', 16.2442727,
												103.2552212 ],
										[ 'สปีดเวย์', 16.2442727, 103.2533320 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2544432 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2555542 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2566652 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2577762 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2588872 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2599982 ],
										[ 'สเต็ก ลุงหนวด', 16.2442727,
												103.2500092 ] ];
								var marker, i, info;
								for (i = 0; i < locations.length; i++) {
									marker = new google.maps.Marker(
											{
												position : new google.maps.LatLng(
														locations[i][1],
														locations[i][2]),
												map : maps,
												title : locations[i][0],
												animation : google.maps.Animation.BOUNCE,
											});

									info = new google.maps.InfoWindow();

									google.maps.event
											.addListener(
													marker,
													'click',
													(function(marker, i) {
														return function() {
															info
																	.setContent(locations[i][0]);
															info.open(maps,
																	marker);
														}
													})(marker, i));
								}
							}
						</script>

						<script
							src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjUeXNsaTcW_mm3dDO_nTTOZl4NraRJfI&callback=myMap&language=th"></script>

					</div>
					<div class="tab-item" id="tab-1" style="padding: 10px;background-color:#F2ABAB;margin-top: -20px">

						<div style="overflow: scroll">
							<table class="table-borders">
								<tr>
									<th style="text-align: center; background-color: #D35783;">
										ชื่อ</th>
									<th style="text-align: center; background-color: #D35783;">
										ที่อยู่</th>
									<th style="text-align: center; background-color: #D35783;">
										โทร</th>
									<th style="text-align: center; background-color: #D35783;">
										พิกัด</th>
									<th style="text-align: center; background-color: #D35783;">
										ลบแก้ไข</th>
									<th style="text-align: center; background-color: #D35783;">
										แก้ใข</th>
								</tr>
								<%
									for (int i = 0; i < 6; i++) {
								%>
								<tr>
									<td>xxx</td>
									<td class=""
										style="text-align: center; background-color: #F4D5E0">xxx</td>
									<td>xxx</td>
									<td class=""
										style="text-align: center; background-color: #F4D5E0">xxx</td>
									<td>xxx</td>
									<td class=""
										style="text-align: center; background-color: #F4D5E0">xxx</td>
								</tr>
								<%
									}
								%>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div style="text-align: center;">
			<a href="${pageContext.request.contextPath}/member/decision/PF352111"
				class="button button-round button-pink-3d button-pink"
				style="width: 80%">เพื่ม</a>
		</div>
	</div>



	<jsp:include page="/include/resources_footer.jsp"></jsp:include>
	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>