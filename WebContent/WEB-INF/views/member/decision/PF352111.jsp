<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Purchase point</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<jsp:include page="/include/resources_js.jsp"></jsp:include>
</head>
<body>
	<div id="header" class="header-logo-app header-dark"
		style="background-image: linear-gradient(to right, black, #AA0846);">
		<a href="${pageContext.request.contextPath}/member/decision/PF352"
			class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title back-button"><strong
			style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> <a
			href="${pageContext.request.contextPath}/member/news/PF2"
			class="header-icon header-icon-2" style="font-size: 20px;"><i
			class="fa fa-home font-24"></i></a> <a href="#"
			class="header-icon header-icon-3"><i class="fa fa-d"
			style="color: black;"></i></a> <a href="#"
			class="header-icon header-icon-4 hamburger-animated no-border"
			data-deploy-menu="menu-1" style="font-size: 20px;"></a>
	</div>

	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	<div class="page-content header-clear-medium" style="padding-top: 50px">
		<div style="margin: 10px">
			<div class="container content" style="margin-top: 20px">
				<div class="blockquote-2 "
					style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
					<img alt=""
						src="/DOEA-FARMER/contents/images/icons8-combo-chart-100.png">
					<h4
						style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">
						เพิ่มจุดรับซื้อ</h4>
				</div>
			</div>
			<div class="border border-danger">
				<div style="margin: 10px">
					<table style="margin-bottom: 5px; border: 0px">
						<tr>
							<td style="padding-right: 10px; border: 0px">ประเภท</td>
							<td style="padding-right: 10px; border: 0px"><input
								class="form-control" type="text" placeholder="xxxxxxx"></td>
							<td style="border: 0px">พืช</td>
							<td style="padding-left: 10px; border: 0px"><input
								class="form-control" type="text" placeholder="xxxxxxx"
								style="position: 10px"></td>
						</tr>
					</table>
					<div style="text-align: center">
						<table style="margin-bottom: 5px; border: 0px">
							<tr>
								<td style="border: 0px">ชื่อ</td>
								<td style="border: 0px"><input class="form-control"
									type="text" placeholder="xxxxxxx"></td>

							</tr>
							<tr>
								<td style="border: 0px">ที่อยู่</td>
								<td style="border: 0px"><input class="form-control"
									type="text" placeholder="xxxxxxx"></td>
							</tr>
							<tr>
								<td style="border: 0px">เบอร์โทร</td>
								<td style="border: 0px"><input class="form-control"
									type="text" placeholder="xxxxxxx"></td>
							</tr>
							<tr>
								<td style="border: 0px">พิกัด</td>
								<td style="border: 0px">
									<div class="input-group mb-2">
										<input type="text" class="form-control"
											id="inlineFormInputGroup" placeholder="xxxxxxx">
										<div class="input-group-prepend">
											<a href="#" class="btn btn-info btn-lg"
												style="padding-top: 3px; padding-bottom: 3px"> <i
												class="fa fa-map-marked-alt"></i>
											</a>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border: 0px">ราคา</td>
								<td style="border: 0px"><input class="form-control"
									type="text" placeholder="xxxxxxx"></td>
							</tr>
						</table>
					</div>
				</div>
				<table style="border: 0px">
					<tr>
						<td style="width: 50%; border: 0px">
							<a type="button" class="btn btn-danger" style="width: 50%" data-deploy-menu="notification-modal-3"
							>เพิ่ม</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="notification-modal-3" data-menu-size="345"
		class="menu-wrapper menu-light menu-modal" style="text-align: center;">
		<div style="padding: 20px; text-align: center;">
			<img alt=""
				src="${pageContext.request.contextPath}/contents/images/icons8-check-file-100.png"
				style="text-align: center; width: 40%; margin-left: 70px;">
			<h4 style="margin-top: 20px;">ยืนยันการเพิ่มจุดรับซื้อ?</h4>
			<table style="background: none; border: none; margin-top: 50px;">
				<tr style="background: none; padding: 0px; border: none;">
					<td
						style="color: white; background: none; padding: 0px; border: none; font-size: 18px; width: 50%;">
						<a
						href="${pageContext.request.contextPath}/member/decision/PF3521"
						class="button button-round button-pink-3d button-pink"
						style="font-size: 18px; background-color: #890016; width: 95%; text-align: center;">
							ยืนยัน </a>
					</td>

					<td
						style="background: none; padding: 0px; border: none; font-size: 18px; width: 50%;">
						<a
						href="${pageContext.request.contextPath}/member/decision/PF352111"
						class="button button-round button-pink-3d button-pink"
						style="font-size: 18px; background-color: #660011; width: 95%; text-align: center;">
							ยกเลิก </a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<jsp:include page="/include/resources_footer.jsp"></jsp:include>
</body>
</html>