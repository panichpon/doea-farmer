<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Manage Point Earnings</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<style>
.tab {
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

.tab button {
	background-color: inherit;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	transition: 0.3s;
}

.tab button:hover {
	background-color: #ddd;
}

.tab button.active {
	background-color: #ccc;
}

.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}
</style>
</head>
<body>
<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF35"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
</div>
<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	<div class="page-content header-clear-medium" style="padding-top: 50px">
		<div style="margin: 10px">
		<div class="container content" style="margin-bottom: 15px;top: 7px;">
			<div class="blockquote-2 "
				style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
				<img alt="" src="/DOEA-FARMER/contents/images/icons8-line-chart-100.png">
				<h4 style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">วิเคราะห์ความเสถียร</h4>
			</div>
		</div>
		</div>
	</div>


		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
		<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>