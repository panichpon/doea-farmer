<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>วิเคราะห์ทางหลังดูประวัติเพื่อเปรียบเทียบ</title>
<style >
.is-loading:disabled{
}
</style>
</head>

<body class=" ">
<!-- 
<jsp:include page="/include/resources_preloader.jsp"></jsp:include>
-->
<div id="page-transitions">
		<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF351"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
		</div>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
<!--
	<jsp:include page="/include/resources_header.jsp"></jsp:include>
	<jsp:include page="/include/resources_menu.jsp"></jsp:include>
-->	
			
			<!-- start -->
			<div id="page-transitions">		
				<div id="page-content" class="page-content">	
					<div id="page-content-scroll"><!--Enables this element to be scrolled --> 
						
						<div class="container content">
							<div class="blockquote-2 "
								style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
								<img alt="" src="/DOEA-FARMER/contents/images/icons8-checked-100-2.png">
								<h4 style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">
									วิเคราะห์ทางเลือกรายได้</h4>
							</div>
						</div>
						
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;border: 2px solid #CCABD8;">
							<div class="homepage-cta" >
							
							<table style="background: none; border: none;margin-bottom: 0px">
								<tr style="background: none; border: none;">
									<td style="background: none; border: none;">
										<img alt="" src="/DOEA-FARMER/contents/images/icons8-time-machine-100.png" style="width: 50px; background-color: #FF9E9E; padding: 5px; border-radius: 50px;">
									</td>
									<td style="background: none; border: none; text-align: left;">
										<h5 class="uppercase ultrabold">	ดูประวัติเพื่อเปรียบเทียบ	</h5>
									</td>
								</tr>
							</table>
								<table style="border: 0px;margin-bottom: 10px">
									<tr>
										<td style="border: 0px"><strong style="font-size: 20px"> จาก </strong></td>
										<td style="border: 0px"> <strong style="font-size: 20px">ถึง</strong> </td>
									</tr>
									<tr>
										<td style="border: 0px;padding-right: 10px;padding-bottom: 0px">
											<div class="select-box select-box-2" style="margin-bottom: 10px">
												<select style="background-image: linear-gradient(to bottom right, #FFCAD4, #F3ACB6);">
													<option value="volvo">วันที่</option>
													<option value="saab">20-07-2019</option>
													<option value="mercedes">21-07-2019</option>
													<option value="audi">22-07-2019</option>
												</select>
											</div>
										</td>
										<td style="border: 0px;padding-left: 10px;padding-bottom: 0px">
											<div class="select-box select-box-2" style="margin-bottom: 10px">
												<select style="background-image: linear-gradient(to bottom right, #FFCAD4, #F3ACB6);">
													<option value="volvo">วันที่</option>
													<option value="saab">20-08-2019</option>
													<option value="mercedes">21-08-2019</option>
													<option value="audi">22-08-2019</option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<input type="date">
										</td>
									</tr>
								</table>
								
								<div style="overflow: scroll">
								<table>
									<tr>
										<th style="text-align: center;background-color: #C35B88;color: white;">	สิ่งที่ทำ	</th>
										<th style="text-align: center;background-color: #C35B88;color: white;">	รายได้</th>
										<th style="text-align: center;background-color: #C35B88;color: white;">	สิ่งที่แนะนำ	</th>
										<th style="text-align: center;background-color: #C35B88;color: white;">	รายได้</th>
									</tr>
									<tr>
										<td	style="background-color: #ECB8CE">หอมแดง</td>
										<td style="background-color: #ECB8CE">200</td>
										<td	style="background-color: #ECB8CE">กระเทียม</td>
										<td style="background-color: #ECB8CE">400</td>
									</tr>
									<tr>
										<td >หอมใหญ่</td>
										<td >400</td>
										<td >กุ้งขาวแวนนาไม</td>
										<td >500</td>
									</tr>		
									<tr>
										<td style="background-color: #ECB8CE">กุ้งขาวแวนนาไม</td>
										<td style="background-color: #ECB8CE">500</td>
										<td style="background-color: #ECB8CE">กระเทียม</td>
										<td style="background-color: #ECB8CE">400</td>
									</tr>
									<tr>
										<td >กระเทียม</td>
										<td >300</td>
										<td >มะนาว</td>
										<td >400</td>
									</tr>		
									<tr>
										<td style="background-color: #ECB8CE">มะนาว</td>
										<td style="background-color: #ECB8CE">400</td>
										<td style="background-color: #ECB8CE">กระเทียม</td>
										<td style="background-color: #ECB8CE">600</td>
									</tr>
									<tr>
										<td >กระเทียม</td>
										<td >400</td>
										<td >น้ำยางสด</td>
										<td >800</td>
									</tr>
									<tr>
										<td style="background-color: #CC95BA">	รายได้สุทธิ์(บาท)	</td>
										<td style="background-color: #E89EC5">4000</td>
										<td style="background-color: #CC95BA">	รายได้สุทธิ์(บาท)	</td>
										<td style="background-color: #E89EC5">6000</td>
									</tr>
								</table>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- end -->
			<jsp:include page="/include/resources_footer.jsp"></jsp:include>
			

	
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>