<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title> วิเคราะห์ด้านการตลาด - จัดการพืชที่สนใจ </title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
</head>
<body>
   
	<div id="page-transitions dark">
		<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF352"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
		</div>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		
			<div class="container content" style="margin-top: 80px;">
				<div class="blockquote-2 "
					style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
					<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-combo-chart-100.png">
					<h4 style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">
						วิเคราะห์ด้านการตลาด	</h4>
				</div>
			</div>
		
		<div class="content content-boxed content-boxed-padding" style="border: 2px solid #FF8E8E;">
				<div class="above-overlay">
					<table style="background: none; border: none;">
						<tr style="background: none; border: none;">
							<td style="background: none; border: none;">
								<img alt="" src="${pageContext.request.contextPath}/contents/images/icons8-cauliflower-100.png" 
								style="width: 50px; background-color: #FF9E9E; padding: 5px; border-radius: 50px;">
							</td>
							<td style="background: none; border: none; text-align: left;">
								<h5 class="uppercase ultrabold">	จัดการพืชที่สนใจ	</h5>
							</td>
						</tr>
					</table>
				<!-- 	<div class="decoration full-top"></div>	 -->
					
						<table>
							<tr>
								<td colspan="2" class="color-white font-18" style="background-color: #B2275C; width: 70%; border: 1px solid #89103E;">
									รายการ
								</td>
								<td class="color-white font-18" style="background-color: #B2275C; width: 30%; border: 1px solid #89103E;">
									ลบ / แก้ไข
								</td>
							</tr>
							<tr>
								<td colspan="4" class=" font-18" style="text-align: left; padding-left: 20px; border: 1px solid #89103E;">
									<a class="inner-link-list" href="#">	พืชสวน	</a>
									<ul class="link-list" style="list-style-type:circle; padding-left: 20px; padding-right: 30px;">
										<li>	กวางตุ้ง		<i class="fa fa-cogs float-right"></i><i class="fa fa-trash float-right"></i></li>
										<li>	คะน้า		<i class="fa fa-cogs float-right"></i><i class="fa fa-trash float-right"></i></li>
										<li>	พริก		<i class="fa fa-cogs float-right"></i><i class="fa fa-trash float-right"></i></li>
									</ul>
								</td>
								
							</tr>
							<tr>
								<td colspan="4" class=" font-18" style="text-align: left; padding-left: 20px; padding-right: 30px; border: 1px solid #89103E;">
									<a class="inner-link-list" href="#">	พืชไร่	</a>
									<ul class="link-list" style="list-style-type:circle; padding-left: 20px;">
										<li>	ข้าว		<i class="fa fa-cogs float-right"></i><i class="fa fa-trash float-right"></i></li>
										<li>	ข้าวโพด		<i class="fa fa-cogs float-right"></i><i class="fa fa-trash float-right"></i></li>
									</ul>
								</td>
							</tr>
							
						</table>
						
					
					<div class="content" style="padding-top: 30px; padding-left: 0px; padding-right: 0px;">
						<a href="${pageContext.request.contextPath}/member/decision/PF35221" class="button button-round button-pink-3d button-pink" style=" font-size: 18px; background-color: #890016; width: 95%; text-align: center;">
							เพิ่ม
						</a>
					</div>
				</div>
				<!-- <div class="overlay opacity-90" style="background-color: #F94371;"></div>	 --> 
			</div>	
			
			
		
		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
	</div>

	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>