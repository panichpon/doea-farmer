<%@ page language="java" contentType="text/html; charset=UTF-8"
 pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>วิเคราะห์ทางเลือกรายได้</title>
<style >
.is-loading:disabled{
}
</style>
</head>

<body class=" ">
<!--  	
<jsp:include page="/include/resources_preloader.jsp"></jsp:include>	
-->
<div id="page-transitions">
		<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF35"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
		</div>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
<!-- 
	<jsp:include page="/include/resources_header.jsp"></jsp:include>

	<jsp:include page="/include/resources_menu.jsp"></jsp:include>
-->		
			
			<div id="page-transitions">		
				<div id="page-content" class="page-content" >	
					<div id="page-content-scroll" ><!--Enables this element to be scrolled --> 
					
						<div class="container content">
							<div class="blockquote-2 "
								style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
								<img alt=""
									src="/DOEA-FARMER/contents/images/icons8-checked-100-2.png">
								<h4
									style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">
									วิเคราะห์ทางเลือกรายได้</h4>
							</div>
						</div>
						
					
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;border: 2px solid #CCABD8;" >
							<div class="homepage-cta" >
								<div style="overflow: scroll">
								<table>
									<tr>
										<th style="text-align: center;background-color: #C35B88;color: white;">	กิจกรรม	</th>
										<th style="text-align: center;background-color: #C35B88;color: white;">	รายได้	</th>
									</tr>
									<tr>
										<td	style="background-color: #ECB8CE">หอมแดง</td>
										<td style="background-color: #ECB8CE">20</td>
									</tr>
									<tr>
										<td >หอมใหญ่</td>
										<td >30</td>
									</tr>		
									<tr>
										<td style="background-color: #ECB8CE">กระเทียม</td>
										<td style="background-color: #ECB8CE">50</td>
									</tr>
									<tr>
										<td >กุ้งขาวแวนนาไม</td>
										<td >80</td>
									</tr>		
									<tr>
										<td style="background-color: #ECB8CE">มะนาว</td>
										<td style="background-color: #ECB8CE">100</td>
									</tr>
								</table>
								</div>
								<a href="${pageContext.request.contextPath}/member/decision/PF3511" class="button button-full button-round " 
								style="font-size: 20px;background-color: #A66387">	ดูประวัติเพื่อเปรียบเทียบ	</a>
							</div>
						</div>			
						
								
						
					</div>
				</div>
			</div>	
			<!--  -->
			<jsp:include page="/include/resources_footer.jsp"></jsp:include>
			

	
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>