<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>Manage Point Earnings</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<style>
.tab {
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

.tab button {
	background-color: inherit;
	float: left;
	border: none;
	outline: none;
	cursor: pointer;
	padding: 14px 16px;
	transition: 0.3s;
}

.tab button:hover {
	background-color: #ddd;
}

.tab button.active {
	background-color: #ccc;
}

.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}
</style>
</head>
<body>
<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
			<a href="${pageContext.request.contextPath}/member/decision/PF35"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
			<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
			<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
			<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
			<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
</div>
<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	<div class="page-content header-clear-medium" style="padding-top: 50px">
		<div style="margin: 10px">
		<div class="container content" style="margin-bottom: 15px;top: 7px;">
			<div class="blockquote-2 "
				style="background-image: linear-gradient(to bottom right, #FAB978, #CCABD8); padding-bottom: 17px; border-radius: 50px;">
				<img alt="" src="/DOEA-FARMER/contents/images/icons8-truck-100.png">
				<h4 style="text-align: center; padding-top: 25px; padding-left: 20px; color: black;">การวิเคราะห์โลจิสติกส์</h4>
			</div>
		</div>
					
			<div class="content" style=" border: 3px solid #FEC0CE;">
 			<div class="content" style="margin: 10px">
			<div class="one-third">
					<form>
					<div class="form-group">
						<select class="form-control" id="exampleFormControlSelect1">
							<option selected>ประเภท</option>
							<option>1</option>
							<option>2</option>

						</select>
					</div>
					</form>
			</div>
			<div class="one-third">
					<form>
					<div class="form-group">
						<select class="form-control" id="exampleFormControlSelect1">
							<option selected>พืช</option>
							<option>1</option>
							<option>2</option>

						</select>
					</div>
					</form>
			</div>
			<div class="one-third last-column">
					<form>
					<div class="form-group">
						<select class="form-control" id="exampleFormControlSelect1">
							<option selected>ผลไม้</option>
							<option>1</option>
							<option>2</option>

						</select>
					</div>
					</form>
			</div>
			
			
			<div class="one-half">
				<div class="form-group">
					<input type="pri" class="form-control" id="#" placeholder="ปริมาณผลผลิต" name="#">
				</div>
			</div>
			<div class="one-half last-column">
				<div class="form-group">
					<input type="pric" class="form-control" id="#" placeholder="ราคาน้ำมัน" name="#">
				</div>
			</div>
			
			<div align="center">
				<input type="ori" class="form-control" id="#" placeholder="ต้นทุนสะสม" name="#">
			</div>
			</div>
			</div>
			
				<div class="content" style="margin-top: 20px">
				<div class="tabs">
					<div class="tab-pill-titles" data-active-tab-pill style="margin-bottom: 0px">
						<a href="#" class="active-tab-pill-button" data-tab-pill="tab-pill-1" style="background-color: #EDCCED;width: 50%">ดูแผนที่</a>
						<a href="#" data-tab-pill="tab-pill-2" style="background-color: #AB87C2;width: 50%">ดูรายชื่อผู้ซื้อ</a>
						
					</div>
					<div class="tab-pill-content">
					
						<div class="tab-item active-tab" id="tab-pill-1">
							<div align="center">
								<div id="googleMapID" style="width: 100% !important; height: 200px !important; border: 3px solid #EDCCED;"></div>

								<script>
									function myMap() {
										var myLatLng = {
											lat : 16.2442727,
											lng : 103.2548162
										};
										var maps = new google.maps.Map(document.getElementById('googleMapID'),
										{
											zoom : 15,
											center : myLatLng,
											mapTypeId : google.maps.MapTypeId.HYBRID
										});
										var locations = [ [ 'วัดลาดปลาเค้า', 16.2442727, 103.2511102 ],
														[ 'หมู่บ้านอารียา', 16.2442727, 103.2552212 ],
														[ 'สปีดเวย์', 16.2442727, 103.2533320 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2544432 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2555542 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2566652 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2577762 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2588872 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2599982 ],
														[ 'สเต็ก ลุงหนวด', 16.2442727, 103.2500092 ] ];
										var marker, i, info;
										for (i = 0; i < locations.length; i++) {
											marker = new google.maps.Marker({
												position : new google.maps.LatLng(locations[i][1],
												locations[i][2]),
											map : maps,
											title : locations[i][0],
											animation : google.maps.Animation.BOUNCE,
										});

										info = new google.maps.InfoWindow();

										google.maps.event.addListener(marker, 'click',
											(function(marker, i) {
												return function() {
													info.setContent(locations[i][0]);
													info.open(maps, marker);
												}
											})(marker, i));
										}
									}
								</script>

								<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnFRmdzH0NXtuC0_Bf0CoisHcrd-9Jqc8&callback=myMap&language=th"></script>

						</div>
						</div>
						<div class="tab-item" id="tab-pill-2">
							<table class="table table-dark table-hover" style="width: 100%; margin-bottom: 10px; border: 3px solid #AB87C2;">
    							<thead>
     								<tr align="center">
        								<th>ชื่อ</th>
        								<th>รายละเอียด</th>
        								<th>ราคา</th>
        		
      								</tr>
    							</thead>
    							<tbody>
      								<tr>
        								<td></td>
        								<td></td>
        								<td></td>
      								</tr>
      								<tr>
        								<td></td>
        								<td></td>
        								<td></td>
      								</tr>
      								<tr>
        								<td></td>
        								<td></td>
        								<td></td>
      								</tr>
    							</tbody>
  							</table>
  							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
		<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>
</html>