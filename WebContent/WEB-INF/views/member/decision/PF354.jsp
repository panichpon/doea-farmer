<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title> ความยุติธรรม (กลุ่ม) </title>
</head>

<body>
	
<jsp:include page="/include/resources_preloader.jsp"></jsp:include>
	
<div id="page-transitions">
	<div id="header" class="header-logo-app header-dark" style="background-image: linear-gradient(to right, black , #AA0846);">
		<a href="${pageContext.request.contextPath}/member/decision/PF35"	class="header-icon back-button"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title back-button"><strong style="color: white; font-size: 20px;">Farmer Mobile App</strong></a> 
		<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2" style="font-size: 20px;"><i class="fa fa-home font-24"></i></a>
		<a href="#" class="header-icon header-icon-3"><i class="fa fa-d" style="color: black;"></i></a>
		<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1" style="font-size: 20px;"></a>
	</div>
	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	
	
			
			<!--  -->	
			<div id="page-transitions">		
				<div id="page-content" class="page-content">	
					<div id="page-content-scroll"><!--Enables this element to be scrolled --> 
						
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;background-color: #8F4365;">
							<div class="homepage-cta" >
								
								<h4 class="color-white"><i class="fa fa-balance-scale"></i>  ความยุติธรรม(กลุ่ม) </h4>
								
							</div>
						</div>
						<div class="content content-boxed content-boxed-padding" style="margin-top: 10px;margin-bottom : 20px;">
							<div class="homepage-cta" >
								<h6>วิเคราะห์ความยุติธรรมในกลุ่ม</h6>
							</div>
						</div>
			
					</div>
				</div>
			</div>
			<!--  -->
			<jsp:include page="/include/resources_footer.jsp"></jsp:include>
			
		
	
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>