<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Home Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>

</head>
<body>
<div id="page-transitions" class="back-button-clicked back-button-not-clicked" >
<div id="header" class="header-logo-app header-dark" style="background: linear-gradient(to right, black, #008000);">
		<a href="#" class="header-icon back-button header-icon-1 font-10 no-border"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title ">Farmer Mobile Application</a>
		<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2 font-14"><i class="fa fa-home"></i></a>
		<a href="#" class="header-icon header-icon-4 hamburger-animated" data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em class="hm3"></em></a>
	</div>	
</div>  
	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	
	<div id="page-content" class="page-content">	
		<div align="center" style="margin-top: 20px;color: white;">
				<a href="#" class="button button-round button-green-3d button-green" style="width: 50%;font-size: 25px;font-weight:bold ;color: black;">เรื่องด่วน</a>
		</div>					
		<div id="page-content-scroll" class="header-clear-large">
			<div class="content content-boxed content-boxed-padding" style="background-color: #008000;">
				<div class="icon-column">
					<div class="content demo-buttons">
						<a href="#" class="button button-round button-green-3d button-green" style="width: 120px;font-weight:bold ;color: black;">ใหม่</a>
						<a href="${pageContext.request.contextPath}/member/news/PF2_1" class="button button-round button-blue-3d button-blue" style="width: 120px;font-weight:bold ;color: black;">อ่านแล้ว</a>
					</div>
					<div class="content-boxed" style="width: 100%;background-color: #cbf2cb;">
						
						<i class="fa fa-sign-in-alt color-blue-dark " style="margin-left: 10px;"></i>
						<p class="no-bottom">
							<a href="${pageContext.request.contextPath}/member/news/PF21">- xxxxxxxxxxxxxxxx...</a><br>
							<a href="${pageContext.request.contextPath}/member/news/PF21">- xxxxxxxxxxxxxxxx...</a><br>
							<a href="${pageContext.request.contextPath}/member/news/PF21">- xxxxxxxxxxxxxxxx...</a><br>
							<a href="${pageContext.request.contextPath}/member/news/PF21">- xxxxxxxxxxxxxxxx...</a>
						</p>
					</div>		
				</div>
			</div>		
		</div> 
	</div>
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>