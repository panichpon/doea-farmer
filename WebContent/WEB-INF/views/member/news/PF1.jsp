<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>login member</title>
</head>
<body>

<div id="page-transitions">
	<div id="header" class="header-logo-app" style="background-color: #79b40e;">
		<a class="header-title" style="color: black;">Farmer Mobile Application</a>
		<a href="#" class="header-icon back-button header-icon-1 font-10 no-border"><i class="fa fa-chevron-left" style="color: black;"></i></a>
		<a href="#" class="header-icon header-icon-2 no-border font-14" data-deploy-menu="menu-4"><i class="fa fa-home" style="color: black;"></i></a>
		<a href="#" class="header-icon header-icon-4 hamburger-animated" data-deploy-menu="menu-1" style="border-color: #00b33c"></a>
	</div>
	<jsp:include page="/include/resources_menu.jsp"></jsp:include>
	<div id="page-content" class="page-content page-content-full" style="min-height: 762px;">	
		<div id="page-content-scroll"><!--Enables this element to be scrolled --> 	
						
			<div class="cover-item cover-item-full" style="height: 862px; width: 856px; margin-top: 3%;">
				<img src="${pageContext.request.contextPath}/contents/images/bg-login.jpg" style="height: 100%; width: 100%;margin-top: 55px;">
				<div class="cover-content cover-content-center" style="margin-left: -250px; margin-top: -213.75px;">
					<div class="page-login content-boxed content-boxed-padding no-top">
						<img class="login-bg responsive-image no-bottom" src="${pageContext.request.contextPath}/contents/images/bg-home-1.jpg">
						<img class="login-image no-border" src="${pageContext.request.contextPath}/contents/images/farmer.png" alt="img">
						<h3 class="uppercase ultrabold full-top no-bottom color-black">เข้าสู่ระบบ</h3>
						
						<div class="page-login-field half-top">
							<i class="fa fa-user"></i>
							<input type="text" placeholder="บัญชี..">
							<em>(จำเป็น)</em>
						</div>			
						<div class="page-login-field half-bottom">
							<i class="fa fa-lock"></i>
							<input type="password" placeholder="รหัสผ่าน..">
							<em>(จำเป็น)</em>
						</div>
						<div class="page-login-links small-bottom">
							<a class="forgot float-right" href="#"><i class="fa fa-user float-right"></i>สร้างบัญชี</a>
							<a class="create float-left" href="page-forgot.html"><i class="fa fa-eye"></i>ลืมรหัสผ่าน</a>
							<div class="clear"></div>
						</div>
						<a href="${pageContext.request.contextPath}/member/news/PF2" class="button button-green button-full">เข้าสู่ระบบ</a>
					<div>
						<hr>
						<a href="#" class="button button-social half-bottom button-full button-sm button-rounded button-icon facebook-bg">
						<i class="fab fa-facebook"></i>เข้าสู่ระบบด้วยบัญชีเฟสบุ๊ค</a>
						<a href="#" class="button button-social half-bottom button-full button-sm button-rounded button-icon google-bg">
						<i class="fab fa-google"></i>เข้าสู๋ระบบด้วยบัญชี Google</a>
					</div>						
				</div>					
			</div>
<!-- 				<div class="cover-infinite-background"></div> -->
				
			</div>	
		</div>  
	</div>
</div>
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>