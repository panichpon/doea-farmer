<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<title>Epsilon X</title>
</head>

<body>
	
<jsp:include page="/include/resources_preloader.jsp"></jsp:include>
<div id="page-transitions" class="back-button-clicked back-button-not-clicked" >
<div id="header" class="header-logo-app header-dark" style="background: linear-gradient(to right, black, #008000);">
		<a href="#" class="header-icon back-button header-icon-1 font-10 no-border"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title ">Farmer Mobile Application</a>
		<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2 font-14"><i class="fa fa-home"></i></a>
		<a href="#" class="header-icon header-icon-4 hamburger-animated" data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em class="hm3"></em></a>
	</div>	
</div> 
<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	
	<div id="page-content" class="page-content">	
		<div align="center" style="margin-top: 20px;color: white;">
				<a href="#" class="button button-round button-green-3d button-green" style="width: 50%;font-size: 25px;font-weight:bold ;color: black;">เรื่องด่วน</a>
		</div>
		<div id="page-content-scroll"><!--Enables this element to be scrolled --> 			
			<div align="center" style="margin: ">
</div>

<div style="margin: 3%">
	<p>เรื่อง : xxxxx</p>
	<p>วันที่ : DD/MM/YY</p>
	<p>ผู้ดำเนินการ : xxxxx</p>
	<p>สถานที่ : xxxxx</p>
	<p>รายละเอียด : xxxxxxxxxxxxxxxxxx</p>
	<br>
	<div align="center">
	<a href="#" class="button button-round button-blue-3d button-blue">ดำเนินการ</a><br>
	<a href="${pageContext.request.contextPath}/member/news/PF2" class="button button-round button-green-3d button-green">กลับ</a> &nbsp; <a href="#" class="button button-round button-green-3d button-green">เก็บไว้ก่อน</a> &nbsp; <a href="#" class="button button-round button-green-3d button-green">ยกเลิก</a>
	</div>
</div>
		
			
		</div>  
	</div>
	
	<jsp:include page="/include/resources_notify.jsp"></jsp:include>
	
	<jsp:include page="/include/resources_profiles.jsp"></jsp:include>

<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>