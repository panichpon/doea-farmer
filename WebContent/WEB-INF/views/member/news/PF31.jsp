<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="bootstrap-4.3.1-dist/js/bootstrap.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" 
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>ข่าวสารการอบรมทางการเกษตร</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include> 
<jsp:include page="/include/resources_js.jsp"></jsp:include>
<style>
h6 {
  text-decoration: underline;
} 
</style>
</head>
<body background="src=${pageContext.request.contextPath}/contents/images/pictures/65077014_618178325328703_1700382792346501120_n.jpg" >
<!-- preloading screen -->
  
<!-- main screen -->
<!-- Nav bar -->
<div id="page-transitions" class="back-button-clicked back-button-not-clicked" >
<div id="header" class="header-logo-app header-dark" style="transition: all 350ms ease 0s; background-color: #008000;" >
		<a href="#" class="header-icon back-button header-icon-1 font-10 no-border"><i class="fa fa-chevron-left"></i></a>
		<a href="#" class="header-title ">Farmer Mobile Application</a>
		<a href="${pageContext.request.contextPath}/member/news/PF2" class="header-icon header-icon-2 font-14"><i class="fa fa-home"></i></a>
		<a href="#" class="header-icon header-icon-4 hamburger-animated" data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em class="hm3"></em></a>
	</div>	
</div>    
<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
<!-- Content -->     
<div class="content" style="margin-top:20%">      
				<h4 class="uppercase ultrabold center-text" style="background-color:#afedaf; border-radius: 10px ">ข้อมูลข่าวสาร </h4>     
				<br>    
				<div class="accordion accordion-style-1 ">
				   
					<a href="#" class="color-black font-14" data-accordion="accordion-1">ชวนเที่ยวเกษตรเพื่อการเรียนรู้ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;11/06/2562 <i class="fa fa-angle-down"></i></a>
					<div class="accordion-content" id="accordion-1" style="display: none; background-color: #cbf2cb; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;"> 
						<h6> รายละเอียดโดยย่อ</h6> 
						<p><b>โครงการ : </b>ชวนเที่ยวเกษตรเพื่อการเรียนรู้โครงการศึกษาดูงานด้านไม้ผลและวิถีชุมชนลุ่มแม่น้ำนครชัยศรี</p>      
						<p><b>วันที่ : </b>11 มิถุนายน พ.ศ.2562</p>
						<p><b>สถานที่ : </b>มหาวิทยาลัยเกษตรศาสตร์</p> 
						<img data-src="${pageContext.request.contextPath}/contents/images/pictures/tour_kaset62.png" src="${pageContext.request.contextPath}/contents/images/pictures/tour_kaset62.pngs" width="700" class="responsive-image preload-image">
					</div>
					<a href="#" class="color-black font-14" data-accordion="accordion-2">อบรมทางการเกษตร &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15/07/2562 <i class="fa fa-angle-down"></i></a>
					<div class="accordion-content" id="accordion-2" style="display: none; background-color: #cbf2cb; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
						<h6>รายละเอียดโดยย่อ</h6>
						<p><b>โครงการ : </b></p>   
						<p><b>วันที่ : </b>15 กรกฎาคม พ.ศ.2562</p>
						<p><b>สถานที่ : </b></p>
					</div> 
					<a href="#" class="color-black font-14" data-accordion="accordion-3"> สมนาเกษตรกร &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22/07/2562 <i class="fa fa-angle-down"></i></a>
					<div class="accordion-content" id="accordion-3" style="display: none;  background-color: #cbf2cb; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
						<h6>รายละเอียดโดยย่อ</h6>
						<p><b>โครงการ : </b></p>   
						<p><b>วันที่ : </b>22 กรกฎาคม พ.ศ.2562</p>
						<p><b>สถานที่ : </b></p>
						 <iframe width="560" height="315" src="https://www.youtube.com/embed/E8gy0WrQTOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
					<a href="#" class="color-black font-14" data-accordion="accordion-4"> อบรมทางการเกษตร &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12/08/2562 <i class="fa fa-angle-down"></i></a>
					<div class="accordion-content" id="accordion-4" style="display: none;  background-color: #cbf2cb; border-bottom-right-radius: 20px; border-bottom-left-radius: 20px;">
						<h6>รายละเอียดโดยย่อ</h6>
						<p><b>โครงการ : </b></p>   
						<p><b>วันที่ : </b>12 สิงหาคม พ.ศ.2562</p>
						<p><b>สถานที่ : </b></p>
					</div>
				</div>
</div>
<br><br><br><br><br><br><br><br><br><br> 
 



 

 	

<div class="footer" style="transition: all 350ms ease 0s; background-image: linear-gradient(to left, black, #008000); transition: all 350ms ease 0s;">	
				<p class="copyright-text" style="color: white">Copyright © MISL <span id="copyright-year">2019</span>. All Rights Reserved.</p>
			</div>
</body>
</html>