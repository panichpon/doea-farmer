<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>PF3.2</title>
</head>
<jsp:include page="/includeFromNeed/resouces_css.jsp"></jsp:include>
<body>

	<jsp:include page="/includeFromNeed/resouces_preloader.jsp"></jsp:include>

	<div id="page-transitions">
		<jsp:include page="/includeFromNeed/resouces_headerback.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resouces_menu.jsp"></jsp:include>

		<div id="page-content" class="page-content">
			<div id="page-content-scroll">
				<!--Enables this element to be scrolled -->

				<div class="decoration decoration-margins"></div>
				<div class="content content-boxed content-boxed-padding color-white"
					style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
					<div class="above-overlay" style="text-align: center;">
						<div class="relative">
							<h3><span class="fas fa-seedling"></span>
								ความต้องการสินค้าการเกษตร
							</h3>
						</div>
					</div>
				</div>


				<div class="content content-boxed content-boxed-padding bg-white"
					style="border: solid 2px black;">
					<div class="above-overlay" style="text-align: center;">
						<h4>วันที่ 19 เดือน มิถุนายน พ.ศ. 2562 เเหล่ง ตลาดไท</h4>
					</div>
				</div>

				<div class="content content-boxed content-boxed-padding"
					id="searchdiv" style="border: solid 2px black;">

					<h5 class="toggle-title ultrabold uppercase" style = "font-size:20px;"> ประเภทพืชไร่ </h5>
					<a href="#" class="toggle-classic toggle-trigger">
						<i class="fa fa-chevron-down"></i></a>
					<div class="toggle-content" style="display: block;">
						<form method="GET" id="searchform" action="/member/cow/search"
							onsubmit="return searchcheck()">
							<div class="typeahead__container">
								<div class="typeahead__field">
									<div class="typeahead__query">

										<b style = "font-size:16px"> 1. ข้าวโพดข้าวเหนียว - เบอร์กลาง 10-11 บาท </b>
										<hr>

										<b style = "font-size:16px"> 2. ข้าวโพดข้าวเหนียว - เบอร์เล็ก 8-10 บาท </b>
										<hr>

										<b style = "font-size:16px"> 3. ข้าวโพดข้าวเหนียว - เบอร์ใหญ่ 14-15 บาท </b>
										<hr>

										<b style = "font-size:16px"> 4. ข้าวโพดหวาน - เบอร์กลาง 10-11 บาท </b>
										<hr>

										<b style = "font-size:16px"> 5. ข้าวโพดหวาน - เบอร์เล็ก 6-8 บาท </b>
										<hr>

										<b style = "font-size:16px"> 6. ข้าวโพดหวาน - เบอร์ใหญ่ 12-12 บาท </b>

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="content content-boxed content-boxed-padding"
					id="searchdiv" style="border: solid 2px black;">

					<h5 class="toggle-title ultrabold uppercase" style = "font-size:20px">  ผลไม้ฤดูกาล   </h5>
					<a href="#" class="toggle-classic toggle-trigger"><i
						class="fa fa-chevron-down"></i></a>
					<div class="toggle-content" style="display: block;">
						<form method="GET" id="searchform" action="/member/cow/search"
							onsubmit="return searchcheck()">
							<div class="typeahead__container">
								<div class="typeahead__field">
									<div class="typeahead__query">

										<b style = "font-size:16px"> 1. กระท้อนทับทิม – เบอร์กลาง     </b>
										<hr>

										<b style = "font-size:16px"> 2. กระท้อนทับทิม – เบอร์เล็ก </b>
										<hr>

										<b style = "font-size:16px"> 3. กระท้อนทับทิม – เบอร์ใหญ่ </b>
										<hr>

										<b style = "font-size:16px"> 4. กระท้อนปุยฝ้าย – เบอร์กลาง </b>
										<hr>

										<b style = "font-size:16px"> 5. กระท้อนปุยฝ้าย – เบอร์เล็ก </b>
										<hr>

										<b style = "font-size:16px"> 6. กระท้อนปุยฝ้าย – เบอร์ใหญ่ </b>

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="content">
					<div class="one-half">
						<a href="${pageContext.request.contextPath}/member/need/PF322"
							style="border: solid 3px #005c99; background-color:#008ae6; font-size:16px;"
							class="button button-full button-round uppercase ultrabold color-white">
							จัดการ </a>
					</div>
					<div class="one-half last-column">
						<a href="${pageContext.request.contextPath}/member/need/PF321"
							style="border: solid 3px #005c99; background-color:#008ae6; font-size:16px;"
							class="button button-full button-round uppercase ultrabold color-white">
							เปรียบเทียบ </a>
					</div>
				</div>
				<div class="decoration decoration-margins"></div>
				<jsp:include page="/includeFromNeed/resouces_footer.jsp"></jsp:include>
			</div>
		</div>
		<jsp:include page="/includeFromNeed/resouces_notify.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resouces_profiles.jsp"></jsp:include>
		<a href="#" class="back-to-top-badge back-to-top-small"><i
			class="fa fa-angle-up"></i>Back to Top</a>
	</div>

	<jsp:include page="/includeFromNeed/resouces_js.jsp"></jsp:include>

</body>