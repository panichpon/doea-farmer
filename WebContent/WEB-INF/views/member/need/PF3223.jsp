<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>PF3.2</title>
</head>
<jsp:include page="/includeFromNeed/resouces_css.jsp"></jsp:include>
<body>

	<jsp:include page="/includeFromNeed/resouces_preloader.jsp"></jsp:include>

	<div id="page-transitions">
		<jsp:include page="/includeFromNeed/resouces_headerback.jsp"></jsp:include>

		<jsp:include page="/includeFromNeed/resouces_menu.jsp"></jsp:include>

		<div id="page-content" class="page-content">
			<div id="page-content-scroll">
				<!--Enables this element to be scrolled -->
				<div class="decoration decoration-margins"></div>
				<div class="content content-boxed content-boxed-padding color-white"
					style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
					<div class="above-overlay" style = "text-align:center;">
						<div class="relative">
							<h3> <span class="fas fa-seedling"></span> รายการสินค้าที่ติดตาม </h3>
						</div>
					</div>
				</div>
				
				<div class="content content-boxed content-boxed-padding" id="searchdiv"
					style="border: solid 2px black;">
					<h5 class="toggle-title ultrabold uppercase" style = "font-size:20px"> ประเภทพืชไร่ </h5>
					<a href="#" class="toggle-classic toggle-trigger"><i
						class="fa fa-chevron-down"></i></a>
					<div class="toggle-content" style="display: block;">
						<form method="GET" id="searchform" action="/member/cow/search"
							onsubmit="return searchcheck()">
							<div class="typeahead__container">
								<div class="typeahead__field">
									<div class="typeahead__query">

										<table class="table-striped">
											<tbody>
												<tr>
													<td>1. ข้าวโพดข้าวเหนียว - เบอร์กลาง 10-11 บาท</td>
													<td>19/01/62</td>
												</tr>
												<tr>
													<td>2. ข้าวโพดข้าวเหนียว - เบอร์เล็ก 8-10 บาท</td>
													<td>19/01/62</td>
												</tr>
												<tr>
													<td>3. ข้าวโพดข้าวเหนียว - เบอร์ใหญ่ 14-15 บาท</td>
													<td>19/01/62</td>
												</tr>
												<tr>
													<td>4. ข้าวโพดหวาน - เบอร์กลาง 10-11 บาท</td>
													<td>19/01/62</td>
												</tr>
												<tr>
													<td>5. ข้าวโพดหวาน - เบอร์เล็ก 6-8 บาท</td>
													<td>19/01/62</td>
												</tr>
												<tr>
													<td>6. ข้าวโพดหวาน - เบอร์ใหญ่ 12-12 บาท</td>
													<td>19/01/62</td>
												</tr>
											</tbody>
										</table>

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<div class="content content-boxed content-boxed-padding" id="searchdiv"
					style="border: solid 2px black;">
					<h5 class="toggle-title ultrabold uppercase" style = "font-size:20px"> ประเภทพืชไร่  </h5>
					<a href="#" class="toggle-classic toggle-trigger"><i
						class="fa fa-chevron-down"></i></a>
					<div class="toggle-content" style="display: block;">
						<form method="GET" id="searchform" action="/member/cow/search"
							onsubmit="return searchcheck()">
							<div class="typeahead__container">
								<div class="typeahead__field">
									<div class="typeahead__query">
										<table class="table-striped">
											<tr>
												<td>1. กระท้อนทับทิม – เบอร์กลาง</td>
												<td>19/01/62</td>

											</tr>
											<tr>
												<td>2. กระท้อนทับทิม – เบอร์เล็ก</td>
												<td>19/01/62</td>

											</tr>
											<tr>
												<td>3. กระท้อนทับทิม – เบอร์ใหญ่</td>
												<td>19/01/62</td>

											</tr>
											<tr>
												<td>4. กระท้อนปุยฝ้าย – เบอร์กลาง</td>
												<td>19/01/62</td>

											</tr>
											<tr>
												<td>5. กระท้อนปุยฝ้าย – เบอร์เล็ก</td>
												<td>19/01/62</td>

											</tr>
											<tr>
												<td>6. กระท้อนปุยฝ้าย – เบอร์ใหญ่</td>
												<td>19/01/62</td>

											</tr>
										</table>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
			<jsp:include page="/includeFromNeed/resouces_footer.jsp"></jsp:include>
		</div>

		<jsp:include page="/includeFromNeed/resouces_notify.jsp"></jsp:include>

		<jsp:include page="/includeFromNeed/resouces_profiles.jsp"></jsp:include>

		<a href="#" class="back-to-top-badge back-to-top-small"><i
			class="fa fa-angle-up"></i>Back to Top</a>
	</div>

	<jsp:include page="/includeFromNeed/resouces_js.jsp"></jsp:include>

</body>