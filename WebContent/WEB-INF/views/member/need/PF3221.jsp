<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

tb, td {
	border: 2px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>PF3.2</title>
</head>
<jsp:include page="/includeFromNeed/resouces_css.jsp"></jsp:include>
<body>

	<jsp:include page="/includeFromNeed/resouces_preloader.jsp"></jsp:include>

	<div id="page-transitions">
		<jsp:include page="/includeFromNeed/resouces_headerback.jsp"></jsp:include>

		<jsp:include page="/includeFromNeed/resouces_menu.jsp"></jsp:include>

		<div id="page-content" class="page-content">
			<div id="page-content-scroll">
				<!--Enables this element to be scrolled -->
				<div class="decoration decoration-margins"></div>
				<div class="content content-boxed content-boxed-padding color-white"
					style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
					<div class="above-overlay" style="text-align: center;">
						<div class="relative">
							<h3><span class="fas fa-seedling "></span> ความต้องการสินค้าการเกษตร-> เพิ่มรายการสินค้าที่จะติดตาม</h3>
						</div>
					</div>
				</div>

				<div class="content content-boxed content-boxed-padding bg-white"
					style="border: solid 2px black;">
					<div class="above-overlay" style="text-align: center;">
						<select class="browser-default custom-select" id="inputGroupSelect04"
							aria-label="Example select with button addon" 
							style = "margin-bottom:10px;">
							<option> ประเภท </option>
							<option value="1">ประเภทพืชไร่</option>
							<option value="2">ประเภทผลไม้</option>
						</select> 

						<select class="browser-default custom-select" id="inputGroupSelect04"
							aria-label="Example select with button addon"
							style = "margin-bottom:10px;">
							<option> พืช </option>
							<option value="1">กระท้อนทับทิม</option>
							<option value="2">ข้าวโพดข้าวเหนียว</option>
							<option value="3">ข้าวโพดหวาน</option>
						</select> 
						<a class="button button-full button-round uppercase ultrabold color-white"
							style="border: solid 3px #005c99; background-color: #008ae6; font-size: 16px;"
							href="#"> เพิ่มรายการ </a>
				
						<table>
							<tr>
								<td>1. กระท้อนทับทิม – เบอร์กลาง</td>
								<td><span class="	fas fa-minus-circle "></span></td>
							</tr>
							<tr>
								<td>2. กระท้อนทับทิม – เบอร์เล็ก</td>
								<td><span class="	fas fa-minus-circle "></span></td>
							</tr>
						</table>
			
						<button type=button class="button button-full button-rounded uppercase ultrabold color-white" 
							style="border: solid 3px #005c99; background-color:#008ae6; font-size:20px;"
							data-toggle="modal" data-target="#myModal">ยืนยัน</button>
						
					</div>
				</div>
				

			</div>


			<jsp:include page="/includeFromNeed/resouces_footer.jsp"></jsp:include>
		</div>

		<jsp:include page="/includeFromNeed/resouces_notify.jsp"></jsp:include>

		<jsp:include page="/includeFromNeed/resouces_profiles.jsp"></jsp:include>

		<a href="#" class="back-to-top-badge back-to-top-small"><i
			class="fa fa-angle-up"></i>Back to Top</a>
	</div>

	<jsp:include page="/includeFromNeed/resouces_js.jsp"></jsp:include>
	<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-lg modal-dialog-centered">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title" style= "text-align:center">  ท่านต้องการจะยืนยันการบันทึการติดตามสินค้าหรือไม่?    </h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" style= "background-color:#33cc33; font-size:16px;"
						class="button button-full button-rounded uppercase ultrabold color-white"
						data-dismiss="modal"><span class="fa-fw select-all fas"></span> ยืนยัน  </button>
					<button type="button" style= "background-color:#ff3333; font-size:16px;"
						class="button button-full button-rounded uppercase ultrabold color-white"
						data-dismiss="modal"><span class="fa-fw select-all fas"></span> ยกเลิก   </button>
				</div>
			</div>
		</div>
	</div>	
</body>