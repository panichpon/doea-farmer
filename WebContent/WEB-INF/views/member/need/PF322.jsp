<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>PF3.2</title>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<jsp:include page="/includeFromNeed/resouces_css.jsp"></jsp:include>
<body>
	
<jsp:include page="/includeFromNeed/resouces_preloader.jsp"></jsp:include>
	
<div id="page-transitions">
	<jsp:include page="/includeFromNeed/resouces_headerback.jsp"></jsp:include>

	<jsp:include page="/includeFromNeed/resouces_menu.jsp"></jsp:include>
	
	<div id="page-content" class="page-content">	
		<div id="page-content-scroll"><!--Enables this element to be scrolled --> 
			<div class="decoration decoration-margins"></div>
			<div class="content content-boxed content-boxed-padding color-white"
				style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
				<div class="above-overlay" style = "text-align:center;">
					<div class="relative">
						<h3> <span class="fas fa-seedling"></span> ความต้องการสินค้าการเกษตร </h3>
					</div>
				</div>
			</div>
				
			<div class="content content-boxed content-boxed-padding bg-white"
				style="border: solid 2px black;">
				<h4 class="uppercase ultrabold small-top color-black"
					style = "text-align:center;"> จัดการ </h4>
				<a class="button button-full button-round uppercase ultrabold color-white"
					style = "border: solid 3px #005c99; background-color:#008ae6; font-size:16px;"
					href="${pageContext.request.contextPath}/member/need/PF3221"><span class="fa-fw select-all fas"></span> เพิ่มรายการสินค้าที่สนใจ  </a>
				<a class="button button-full button-round uppercase ultrabold color-white"
					style = "border: solid 3px #005c99; background-color:#008ae6; font-size:16px;"
					href="${pageContext.request.contextPath}/member/need/PF3222"><span class="fa-fw select-all fas"></span> ลบรายการสินค้าที่สนใจ   </a>
				<a class="button button-full button-round uppercase ultrabold color-white"
					style = "border: solid 3px #005c99; background-color:#008ae6; font-size:16px;"
					href="${pageContext.request.contextPath}/member/need/PF3223"><span class="fa-fw select-all fas"></span> เเสดงรายการสินค้าปัจจุบัน  </a>
			</div>
			
		</div> 
	
		<jsp:include page="/includeFromNeed/resouces_footer.jsp"></jsp:include> 
	</div>
	
	<jsp:include page="/includeFromNeed/resouces_notify.jsp"></jsp:include>

	<jsp:include page="/includeFromNeed/resouces_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>

<jsp:include page="/includeFromNeed/resouces_js.jsp"></jsp:include>

</body>