<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>PF3.2.1</title>
</head>
<jsp:include page="/includeFromNeed/resouces_css.jsp"></jsp:include>
<body>
	
<jsp:include page="/includeFromNeed/resouces_preloader.jsp"></jsp:include>
	
<div id="page-transitions">
	<jsp:include page="/includeFromNeed/resouces_headerback.jsp"></jsp:include>

	<jsp:include page="/includeFromNeed/resouces_menu.jsp"></jsp:include>
	
	<div id="page-content" class="page-content">	
		<div id="page-content-scroll"><!--Enables this element to be scrolled --> 	
			<div class="decoration decoration-margins"></div>
			
			<div class="content content-boxed content-boxed-padding color-white"
				style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
				<div class="above-overlay" style="text-align:center;">
					<div class="relative">
						<h3> <span class="fas fa-seedling "></span> กราฟ </h3>
					</div>
				</div>
			</div>	
				
			<div align="center" class="content content-boxed content-boxed-padding color-white" 
				style="margin-top: 25px; border: solid 2px black;">
				<div class="content-fullscreen">
                    <div class="content" style="margin:0 auto;max-width:768px">
                    <iframe class="chartjs-hidden-iframe" tabindex="-1" 
                    style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;">
                    </iframe>
                       <canvas id="chartContainer" width="932" height="532" class="chartjs-render-monitor" style="display: block; width: 791px; height: 395px;"></canvas>
                    </div>
                </div>
			</div>
		</div> 
		
		<jsp:include page="/includeFromNeed/resouces_footer.jsp"></jsp:include> 
	</div>
	
	<jsp:include page="/includeFromNeed/resouces_notify.jsp"></jsp:include>

	<jsp:include page="/includeFromNeed/resouces_profiles.jsp"></jsp:include>
	
	<a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
</div>
	<script>
	  var ctx = document.getElementById("chartContainer").getContext('2d');
	  window.onload = function() {
	  var myChart = new Chart(ctx, {
		type: 'line',
	    data: {
	      labels: ["08:00", "09:00", "10:00", "11:00", "12:00", "13:00"],
	      datasets: [{
	        label: '# of Votes',
	        data: [12, 19, 3, 5, 6, 7],
	        fill: false,
	        borderColor: [ 'rgba(54, 162, 235, 1)'],
	        borderWidth: 2
	      },{
	    	  label: '# of Votes2',
		        data: [15, 16, 13, 7, 8, 2],
		        fill: false,
		        borderColor: [ 'rgba(54, 162, 235, 1)'],
		        borderWidth: 2 
	      }]
	  
	    },
	    options: {
	    title: {
	    	display: true,
	    	text: 'สินค้า',
	    	fontSize: 20
	    },
	      scales: {
	        yAxes: [{
	          ticks: {
	            beginAtZero: true
	          }
	        }]
	      }
	    }
	  });
	}
	</script>
<jsp:include page="/includeFromNeed/resouces_js.jsp"></jsp:include>

</body>
</html>