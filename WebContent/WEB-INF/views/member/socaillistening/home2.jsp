<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Social Listening</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
</head>
<body class="light-mode">
	<div id="page-transitions">

		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		<div class="modal" id="myModal" style="margin-top: 300px">
			<div class="modal-dialog">
				<div class="modal-content">

					<div class="modal-body">
						คุณต้องการลบ คีย์เวิรืด หรือไม่?<br> <a href="#"
							class="button button-rounded button-green mt-5">ใช่</a>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		<div id="page-content" class="page-content" style="margin-top: 13px;background-color: #FAEEE6" >
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!--Enables this element to be scrolled -->
					<div align="center">
						<a class="button button-rounded  "
							style="font-size: 26px; width: 90%; background-color: #FF9900;">จัดการ
							Key Word</a>
						<table style="width: 90%; margin-top: 20px">
							<tbody>
								<tr >
									<th class="bg-gray-dark"style="color: #000000">คำสำคัญ</th>
									<th class="bg-gray-dark"style="color: #000000">วันที่</th>
									<th class="bg-gray-dark"style="color: #000000">ลบ</th>
									<th class="bg-gray-dark"style="color: #000000">แก้ไข</th>
								</tr>
								<tr >
									<td class="bg-gray-light"style="color: #000000">อัญชัน</td>
									<td class="bg-green-light"style="color: #000000">15/04/2562</td>
									<td class="bg-green-light"><i class="fa fa-trash-alt "
										data-toggle="modal" data-target="#myModal"style="color: #000000"></i></td>
									<td class="bg-green-light"style="color: #000000"><i class="fas fa-edit"style="color: #000000"></i></td>

								</tr>
								<tr>
									<td class="bg-gray-light"style="color: #000000">ตะไคร้</td>
									<td class="bg-yellow2-dark"style="color: #000000">15/04/2562</td>
									<td class="bg-yellow2-dark"style="color: #000000"><i class="fa fa-trash-alt"
										data-toggle="modal" data-target="#myModal"></i></td>
									<td class="bg-yellow2-dark"><i class="fas fa-edit"style="color: #000000"></i></td>
								</tr>
								<tr>
									<td class="bg-gray-light"style="color: #000000">กระเทียม</td>
									<td class="bg-orange-light"style="color: #000000">15/04/2562</td>
									<td class="bg-orange-light"style="color: #000000"><i
										class="fa fa-trash-alt bg-90" data-toggle="modal"
										data-target="#myModal"style="color: #000000"></i></td>
									<td class="bg-orange-light"><i class="fas fa-edit"style="color: #000000"></i></td>
								</tr>
								<tr>
									<td class="bg-gray-light"style="color: #000000">มะละกอ</td>
									<td class="bg-green-light"style="color: #000000">15/04/2562</td>
									<td class="bg-green-light"style="color: #000000"><i class="fa fa-trash-alt"
										data-toggle="modal" data-target="#myModal"style="color: #000000"></i></td>
									<td class="bg-green-light"><i class="fas fa-edit"style="color: #000000"></i></td>

								</tr>
								<tr>
									<td class="bg-gray-light"style="color: #000000">มะเขือเทศ</td>
									<td class="bg-yellow2-dark"style="color: #000000">15/04/2562</td>
									<td class="bg-yellow2-dark"style="color: #000000"><i class="fa fa-trash-alt"
										data-toggle="modal" data-target="#myModal"></i></td>
									<td class="bg-yellow2-dark"><i class="fas fa-edit"style="color: #000000"></i></td>
								</tr>
								<tr>
									<td class="bg-gray-light"style="color: #000000">กระเจี๊ยบเขียว</td>
									<td class="bg-orange-light"style="color: #000000">15/04/2562</td>
									<td class="bg-orange-light"style="color: #000000"><i
										class="fa fa-trash-alt bg-90" data-toggle="modal"
										data-target="#myModal"style="color: #000000"></i></td>
									<td class="bg-orange-light"><i class="fas fa-edit"style="color: #000000"></i></td>
								</tr>
							</tbody>
						</table>


						<a href="#"
							style="height:100px;width: 200px; background-color: #FF9900; margin-top: 50px; margin-bottom: 50px; color: #000000; font-size: 20px;"
							class="button button-s button-icon regularbold"
							data-deploy-menu="menu-6"><i class="fas fa-plus"></i>เพิ่มคำสำคัญ
						</a>
					</div>
					<jsp:include page="/include/resources_footer.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
</body>
<jsp:include page="/include/resources_js.jsp"></jsp:include>