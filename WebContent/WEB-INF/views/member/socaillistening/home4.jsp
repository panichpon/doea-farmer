<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Social Listening</title>
<jsp:include page="/include/resources_js.jsp"></jsp:include>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/contents/scripts/charts.js"></script>
</head>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
<body class="light-mode">
	<div id="page-transitions">
		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		<div id="page-content" class="page-content" style="margin-top: 13px">
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!--Enables this element to be scrolled -->
					<div align="center">
						<div class="blockquote-2 content"
							style="background-image: linear-gradient(to bottom right, #ffc299, #e65c00); padding-bottom: 14px;">
							<img alt=""
								src="/DOEA-FARMER/contents/images/icons8-combo-chart-100.png">
							<h4
								style="text-align: center; padding-top: 25px; padding-left: 35px; font-size: 23px; color: white;">
								รายละเอียดเพิ่มเติม</h4>
						</div>
						<div class="content content-boxed content-boxed-padding"
							style="margin-top: 10px; margin-bottom: 20px;">
							<table style="margin-top: 10px; width: 90%">
								<tbody>
									<tr>
										<td class="bg-gray-light">คำสำคัญ</td>
										<td style="background-color: #ffe0cc">_____________</td>
									</tr>
									<tr>
										<td class="bg-gray-light">Status1</td>
										<td style="background-color: #ffc299">____________</td>
									</tr>
									<tr>
										<td class="bg-gray-light">Status2</td>
										<td style="background-color: #ffb380">____________</td>
									</tr>
									<tr>
										<td class="bg-gray-light">Detail</td>
										<td style="background-color: #ffa366">_____________</td>
									</tr>
								</tbody>
							</table>
							<img alt=""
								src="/DOEA-FARMER/contents/images/icons8-combo-chart-100-2.png">
							<canvas id="chart1" class="chartjs" width="990" height="495"
								style="display: block; height: 165px; width: 330px;"></canvas>
							<div style="margin-top: 20px"></div>
							<canvas id="chart2" class="chartjs" width="990" height="495"
								style="display: block; height: 165px; width: 330px;"></canvas>
						</div>
						<div style="margin-top: 20px"></div>
						<jsp:include page="/include/resources_footer.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	new Chart(document.getElementById("chart1"),
			{
				"type" : "bar",
				"data" : {
					"labels" : [ "Status1", "Status2", "Status3" ],
					"datasets" : [ {
						"label" : "Status Detail",
						"data" : [ 20, 50, 30 ],
						"fill" : false,
						"backgroundColor" : [ "rgba(255, 99, 132, 0.2)",
								"rgba(255, 159, 64, 0.2)",
								"rgba(255, 205, 86, 0.2)",
								"rgba(75, 192, 192, 0.2)",
								"rgba(54, 162, 235, 0.2)",
								"rgba(153, 102, 255, 0.2)",
								"rgba(201, 203, 207, 0.2)" ],
						"borderColor" : [ "rgb(255, 99, 132)",
								"rgb(255, 159, 64)", "rgb(255, 205, 86)",
								"rgb(75, 192, 192)", "rgb(54, 162, 235)",
								"rgb(153, 102, 255)", "rgb(201, 203, 207)" ],
						"borderWidth" : 1
					} ]
				},
				"options" : {
					"scales" : {
						"yAxes" : [ {
							"ticks" : {
								"beginAtZero" : true
							}
						} ]
					}
				}
			});
</script>
<script>
	new Chart(document.getElementById("chart2"), {
		"type" : "pie",
		"data" : {
			"labels" : [ "Status1", "Status2", "Status3" ],
			"datasets" : [ {
				"label" : "My First Dataset",
				"data" : [ 20, 50, 30 ],
				"backgroundColor" : [ "rgb(255, 99, 132)", "rgb(54, 162, 235)",
						"rgb(255, 205, 86)" ]
			} ]
		}
	});
</script>
</html>