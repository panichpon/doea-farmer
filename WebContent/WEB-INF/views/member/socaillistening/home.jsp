<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Social Listening</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
</head>

<body style="background-color: #757575">
	<div id="page-transitions">
		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
		<div id="page-content" class="page-content"
			style="margin-top: 13px; background-color: #FAEEE6">
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!--Enables this element to be scrolled -->
					<div align="center" class="mt-3">
						<div class="blockquote-2 content"
							style="background-image: linear-gradient(to bottom right, #FFCC00, #FF9900); padding-bottom: 17px; border-radius: 50px;">
							<h4
								style="text-align: center; padding-top: 20px; padding-left: 10px; font-size: 23px; color: Back;">
								Social Listening</h4>
						</div>
						<div class="blockquote-2 content mt-5"
							style="height: 70px; width: 300px; background-image: linear-gradient(to bottom right, #FFFF33, #FF9933); padding-bottom: 17px; border-radius: 50px;">
							<div align="left">
								<i class="fas fa-tasks"></i>
							</div>
							<a
								style="text-align: center; padding-top: 20px; padding-left: 40px; font-size: 23px; color: #000000"
								href="${pageContext.request.contextPath}/member/socaillistening/SocailListening">จัดการ
								Key Word</a>
						</div>
						<div class="blockquote-2 content"
							style="height: 70px; width: 300px; background-image: linear-gradient(to bottom right, #FFFF33, #FF9933); padding-bottom: 17px; border-radius: 50px;">
							<div align="left">
								<i class="fas fa-paper-plane"></i>
							</div>
							<a
								href="${pageContext.request.contextPath}/member/socaillistening//report"
								style="text-align: center; padding-top: 20px; padding-left: 10px; font-size: 23px; color: #000000">รายงาน
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/include/resources_footer.jsp"></jsp:include>
	</div>
	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>