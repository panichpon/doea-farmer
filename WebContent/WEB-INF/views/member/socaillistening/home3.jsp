<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<title>Social Listening</title>
<jsp:include page="/include/resources_css.jsp"></jsp:include>
</head>
<body class="light-mode">
	<div id="page-transitions">
		<jsp:include page="/includesocial/resroucesheader.jsp"></jsp:include>
		<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>

		<div id="page-content" class="page-content" style="margin-top: 13px">
			<div id="page-content-scroll">
				<div id="page-content-scroll">
					<!--Enables this element to be scrolled -->
					<div align="center">
						<div class="blockquote-2 content"
							style="background-image: linear-gradient(to bottom right, #ffc299, #e65c00); padding-bottom: 14px;">
							<img alt=""
								src="/DOEA-FARMER/contents/images/icons8-line-chart-100.png">
							<h4
								style="text-align: center; padding-top: 25px; padding-left: 35px; font-size: 23px; color: white;">
								รายงาน</h4>
						</div>
						<div class="content content-boxed content-boxed-padding"
							style="margin-top: 10px; margin-bottom: 20px;">
							<table style="width: 100%; margin-top: 10px">
								<tbody>
									<tr>
										<th class="bg-gray-dark">Keyword</th>
										<th class="bg-gray-dark">status1</th>
										<th class="bg-gray-dark">status2</th>
										<th class="bg-gray-dark">Detail</th>
									</tr>
									<tr>
										<td class="bg-gray-light">อัญชัน</td>
										<td style="background-color: #ffe0cc">ดี</td>
										<td style="background-color: #ffe0cc">ไม่ดี</td>
										<td style="background-color: #ffe0cc"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
										>เพิ่มเติม</a></td>

									</tr>
									<tr>
										<td class="bg-gray-light">ตะไคร้</td>
										<td style="background-color: #ffc299">ดี</td>
										<td style="background-color: #ffc299">ไม่ดี</td>
										<td style="background-color: #ffc299"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
											>เพิ่มเติม</a></td>
									</tr>
									<tr>
										<td class="bg-gray-light">กระเทียม</td>
										<td style="background-color: #ffb380">ดี</td>
										<td style="background-color: #ffb380">ไม่ดี</td>
										<td style="background-color: #ffb380"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
											>เพิ่มเติม</a></td>
									</tr>
									<tr>
										<td class="bg-gray-light">มะละกอ</td>
										<td style="background-color: #ffa366">ดี</td>
										<td style="background-color: #ffa366">ไม่ดี</td>
										<td style="background-color: #ffa366"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
											>เพิ่มเติม</a></td>

									</tr>
									<tr>
										<td class="bg-gray-light">มะเขือเทศ</td>
										<td style="background-color: #ff944d">ดี</td>
										<td style="background-color: #ff944d">ไม่ดี</td>
										<td style="background-color: #ff944d"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
											>เพิ่มเติม</a></td>
									</tr>
									<tr>
										<td class="bg-gray-light">กระเจี๊ยบเขียว</td>
										<td style="background-color: #ffc299">ดี</td>
										<td style="background-color: #ffc299">ไม่ดี</td>
										<td style="background-color: #ffc299"><a
											href="${pageContext.request.contextPath}/member/socaillistening/reportStatus"
											>เพิ่มเติม</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<jsp:include page="/include/resources_footer.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<jsp:include page="/include/resources_js.jsp"></jsp:include>
</body>