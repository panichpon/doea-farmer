<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Home Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
<jsp:include page="/include/resources_css.jsp"></jsp:include>

</head>
<body style="background-color: #e6ffcc;">
	<div id="header" class="header-logo-app header-dark" style="background: linear-gradient(to right, black, #008000);">
		<a href="#" class="header-title back-button">Farmer Mobile Application</a>
		<a href="" class="header-logo disabled"></a>
<%-- 		<a href="${pageContext.request.contextPath}/" onclick="goBack()" class="header-icon header-icon-1 back-button font-10 no-border"><i class="fa fa-chevron-left"></i></a> --%>
<%-- 		<a href="${pageContext.request.contextPath}/" class="header-icon header-icon-2 no-border font-14" data-deploy-menu="menu-4"><i class="fa fa-home"></i></a> --%>
		<a href="#" class="header-icon header-icon-4 hamburger-animated no-border" data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em class="hm3"></em></a>
	</div>	
	<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
	
	<div id="page-content" class="page-content" style="background-color: #e6ffcc;">	
		<div id="page-content" class="page-content page-content-full" style="min-height: 762px;">	
		<div id="page-content-scroll"><!--Enables this element to be scrolled --> 	
						
			<div class="cover-item cover-item-full" style="height: 50%; width: 40%;  background-color: #e6ffcc;">
				<div class="cover-content cover-content-center" style="margin-left: -250px; margin-top: -213.75px;">
					<div class="page-login content-boxed content-boxed-padding no-top no-bottom">
						
							<h3 align="center" class="uppercase ultrabold full-top no-bottom" style="color: #00cc00">เข้าสู่ระบบ</h3>
						
						<div class="page-login-field half-top">
							<i class="fa fa-user"></i>
							<input type="text" placeholder="บัญชี..">
							<em>(จำเป็น)</em>
						</div>			
						<div class="page-login-field half-bottom">
							<i class="fa fa-lock"></i>
							<input type="password" placeholder="รหัสผ่าน..">
							<em>(จำเป็น)</em>
						</div>
						<div class="page-login-links small-bottom">
							<a class="forgot float-right" href="#"><i class="fa fa-user float-right"></i>สร้างบัญชี</a>
							<a class="create float-left" href="page-forgot.html"><i class="fa fa-eye"></i>ลืมรหัสผ่าน</a>
<!-- 							<a>asdasdsad &nbsp; &nbsp; asdasdsa</a> -->
							<div class="clear"></div>
						</div>
						<a href="${pageContext.request.contextPath}/member/news/PF2" class="button button-green button-full" style="border-radius: 3px">เข้าสู่ระบบ</a>
					<div>
						<hr>
						<a href="#" class="button button-social half-bottom button-full button-sm button-rounded button-icon facebook-bg">
						<i class="fab fa-facebook"></i>เข้าสู่ระบบด้วยบัญชีเฟสบุ๊ค</a>
						<a href="#" class="button button-social half-bottom button-full button-sm button-rounded button-icon google-bg">
						<i class="fab fa-google"></i>เข้าสู่ระบบด้วยบัญชี Google</a>
					</div>			
				</div>			
			</div>
			</div>	
			
		</div>  
	</div>
	
</div>
<jsp:include page="/include/resources_js.jsp"></jsp:include>

</body>