<%@page import="misl.spring.model.TbMemberModel"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Member View</title>
<%
	@SuppressWarnings("unchecked")
	ArrayList<TbMemberModel> memberList = (ArrayList<TbMemberModel>) request.getAttribute("memberList");
%>
</head>
<body>
	<h1>Member View</h1>
	<hr />

	<table style="width: 100%" border="1">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Surname</th>
			<th>Email</th>
			<th>Password</th>
			<th>Address</th>
			<th>Telephone</th>
			<th>Register at Date</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<%
			for (int i = 0; i < memberList.size(); i++) {
				TbMemberModel memberModel = memberList.get(i);
		%>
		<tr>
			<td><%=memberModel.getMemberId()%></td>
			<td><%=memberModel.getMemberName()%></td>
			<td><%=memberModel.getMemberSurname()%></td>
			<td><%=memberModel.getMemberEmail()%></td>
			<td><%=memberModel.getMemberPass()%></td>
			<td><%=memberModel.getMemberAddr()%></td>
			<td><%=memberModel.getMemberTel()%></td>
			<td><%=memberModel.getTimeReg()%></td>
			<td><a href="${pageContext.request.contextPath}/test/member_edit?member_id=<%=memberModel.getMemberId() %>">Edit</a></td>
			<td><a href="${pageContext.request.contextPath}/test/member_delete?member_id=<%=memberModel.getMemberId() %>">Delete</a></td>
		</tr>
		<%
			}
		%>
	</table>






</body>
</html>