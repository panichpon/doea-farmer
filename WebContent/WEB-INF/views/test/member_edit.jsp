<%@page import="misl.spring.model.TbMemberModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Member Edit</title>
<%
TbMemberModel memberModel = (TbMemberModel)request.getAttribute("memberModel");
%>
</head>
<body>
	<h1>Member Edit</h1>
	<hr />

	<form action="${pageContext.request.contextPath}/test/member_edit_confirm" method="post">
		Name:<br> 
		<input type="text" name="name" required="required" value="<%=memberModel.getMemberName() %>"><br>
		Surname:<br> 
		<input type="text" name="surname" required="required" value="<%=memberModel.getMemberSurname() %>"><br> 
		Email:<br> 
		<input type="text" name="email" required="required" value="<%=memberModel.getMemberEmail() %>"><br>
		Password:<br> 
		<input type="password" name="password" required="required" value="<%=memberModel.getMemberPass() %>"><br> 
		Address:<br> 
		<input type="text" name="addr" required="required" value="<%=memberModel.getMemberAddr() %>"><br> 
		Telephone:<br>
		<input type="text" name="tel" required="required" value="<%=memberModel.getMemberTel() %>"><br> 
		<br>
		<input type="hidden" name="member_id" value="<%=memberModel.getMemberId() %>">
		<input type="submit" value="Edit"> | <input type="reset" value="Reset">

	</form>

</body>
</html>