<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="/includeFromNeed/resources_menu1.jsp"></jsp:include>
<div id="header" class="header-logo-app header-light" style = "background-image: linear-gradient(90deg, #007acc, #002e4d);">
		<strong class="header-title color-white" style = "font-size:20px;">Farmer Mobile App</strong>
		<a href="#" class="header"></a>
		<a href="#" class="header-logo disabled"></a>
		<a href="#" class="header-icon back-button header-icon-1 font-10 no-border"><i class="fa fa-chevron-left" style ="color:white;"></i></a>
		<a href="${pageContext.request.contextPath}/member/home" class="header-icon header-icon-2 no-border font-14" ><i class="fa fa-home" style ="color:white;"></i></a>
		<a href="#" class="header-icon header-icon-4" data-deploy-menu="menu-1"><i class="fa fa-bars" style ="color:white;"></i></a>
</div>
<!--  -->