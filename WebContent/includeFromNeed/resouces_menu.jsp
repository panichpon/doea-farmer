<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>	
		<div id="menu-1" class="menu-wrapper menu-light menu-modal-full menu-thumbnails">
		<div class="menu-scroll">
                <a href="${pageContext.request.contextPath}/" class="menu-logo">
                    <em class="menu-logo-image"></em>
                    <em class="menu-logo-text font-18 bold">เมนูหลัก</em>
                </a>
                <div class="menu">
                    <a href="#!"><i class="fa fa-star color-yellow-dark"></i><em class="font-16 bold">ข่าวสาร</em></a>
                    <a href="#!" class="active-item"><i class="fa fa-home color-blue-dark"></i><em class="font-16 bold">หน้าหลัก</em></a>
                    <a href="#!"><i class="fa fa-cog color-gray-dark"></i><em class="font-16 bold">ตั้งค่าระบบ</em></a>
                    <a href="#!"><i class="fa fa-user-plus color-orange-dark"></i><em class="font-16 bold">เพิ่มผู้ใช้งาน</em></a>
                    <a href="#!"><i class="fa fa-university color-green-dark"></i><em class="font-16 bold">ข้อมูลฟาร์ม</em></a>
                    <a href="#!"><i class="fa fa-venus-mars color-red-dark"></i><em class="font-16 bold">จัดการตัวโค</em></a>
                    
                    <a href="#!" style="background-image:url(https://dairy.zyanwoa.com/contents/icon/vet.png);background-position:center;background-size: 40px 40px;background-repeat: no-repeat;background-repeat: no-repeat;background-position-y: 30%;"><em class="font-16 bold">แจ้งสัตว์ป่วย</em></a>
                    <a href="#!"><i class="fa fa-bell color-blue-dark"></i><em class="font-16 bold">แจ้งเตือน</em></a>
                    <a href="#!"><i class="far fa-chart-bar color-night-light"></i><em class="font-16 bold">ประสิทธิภาพ</em></a>
                    <a href="#!"><i class="fas fa-chart-pie color-green-dark"></i><em class="font-16 bold">สถิติฟาร์ม</em></a>
                    
                    <a href="#!"><i class="fa fa-map-marker color-red-light"></i><em class="font-16 bold">พิกัดฟาร์ม</em></a>
                    <a href="#!"><i class="fa fa-microchip"></i><em class="font-16 bold">เซ็นเซอร์ฟาร์ม</em></a>
                    <a href="#!"><i class="fa fa-credit-card fa-fw color-magenta-dark"></i><em class="font-16 bold"> บัญชีฟาร์ม</em></a>
                    <a href="#!"><i class="fa fa-question-circle color-green2-dark"></i><em class="font-16 bold">วิธีการใช้งาน</em></a>
                    <a href="#!" style="background-image:url(https://dairy.zyanwoa.com/contents/icon/emarket.png);background-position:center;background-size: 40px 40px;background-repeat: no-repeat;background-repeat: no-repeat;background-position-y: 30%;"><em class="font-16 bold">ซื้อ/ขาย</em></a>
                    <a class="no-smst" href="#!"><i class="fa fa-unlock-alt color-red-light"></i><em class="font-16 bold">ออกจากระบบ</em></a>
                    <a href="#!" class="close-menu"><i class="fa fa-times color-black"></i><em class="font-16 bold">ปิด</em></a>
                    <div class="clear"></div>
                </div>
            </div>
         </div>