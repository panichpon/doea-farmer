<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<style>
	#tex-f{
		color: white;
	}
</style>
<div class="footer footer-dark" style = "background-image: linear-gradient(90deg, #008ae6, #004d80);">
	<a href="#" class="footer-logo"></a> 
	<p id ="tex-f" class="copyright-text">&copy; Zyntelligent Co., Ltd. <span id="copyright-year">2019</span>.</p>
	<p id ="tex-f" class="copyright-text">All Rights Reserved.</p>
</div>