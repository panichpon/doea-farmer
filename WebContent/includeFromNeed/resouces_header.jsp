<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<div id="header" class="header-logo-left header-light bg-green2-dark">
		<a href="${pageContext.request.contextPath}/" class="header-logo"></a>
		<a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
		<a href="#" class="header-icon header-icon-2 font-14" data-deploy-menu="menu-4"><i class="fas fa-address-book"></i></a>
		<a href="#" class="header-icon header-icon-3 font-13 no-border" data-deploy-menu="menu-2"><i class="fas fa-bell"></i></a>
</div> 