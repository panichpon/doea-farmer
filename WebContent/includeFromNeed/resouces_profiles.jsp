<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- Contact Icon Menu -->
	<div id="menu-4" data-menu-size="385" class="menu-wrapper menu-light menu-bottom menu-contact">
		<div class="menu-scroll">
			<div class="contact-form">
				<div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
					<p class="center-text full-bottom full-top"><i class="fa fa-2x fa-paper-plane-o"></i></p>
					<h4 class="uppercase ultrabold half-bottom center-text">Message Sent</h4>
					<p class="center-text boxed-text half-bottom">
						We usually reply in less than 24 hours. <br/> Thank you for getting in touch with us!
					</p>
					<a href="#" class="button button-round color-white button-blue button-xs button-center close-menu uppercase bold full-bottom full-top">Return to Site</a>
				</div>
				<form action="#" method="post" class="contactForm" id="contactForm">
					<fieldset>
						<div class="menu-contact-title half-top">
							<h4>บันทึกการแจ้งเตือนกิจกรรม</h4>
							<h5 class="full-bottom">ระบบบันทึกการแจ้งเตือนกิจกรรม สำหรับผู้ใช้ที่ต้องการการแจ้งเตือนกิจกรรมรายวันโดยสามารถบันทึกได้ไม่จำกัด..</h5>
						</div>
						<div class="formValidationError" id="contactDateFieldError">กรุณากรอกวันที่</div>
						<div class="formValidationError" id="contactTitleFieldError">กรุณากรอกชื่อเรื่อง</div>
						<div class="formValidationError" id="contactMessageTextareaError">โปรดระบุรายละเอียด</div>
						<div class="formFieldWrap">
							<label>วันที่</label>
							<input type="date" name="date" placeholder="วันเดือนปี" value="" class="contactField requiredField" id="contactDateField" />
						</div>
						<div class="formFieldWrap">
							<label>ชื่อเรื่อง</label>
							<input type="text" name="namehead" placeholder="ชื่อเรื่อง" value="" class="contactField requiredField requiredTitleField" id="contactTitleField" /> 
						</div>
						<div class="clear"></div>
						<div class="formTextareaWrap half-bottom">
							<label>รายละเอียด</label>
							<textarea name="contactMessageTextarea" placeholder="ระบุรายละเอียดที่จะบันทึก" class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
						</div>
						<div class="contactFormButton">
							<input type="submit" class="buttonWrap contactSubmitButton" id="contactSubmitButton" value="บันทึก" data-formId="contactForm" />
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>	