<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div id="header" class="header-logo-app header-dark"
	style="transition: all 350ms ease 0s; background-image: linear-gradient(to right, black, rgb(10, 120, 70)); transition: all 350ms ease 0s;">
	<a href="#" class="header-title  back-button" style="font-size: 150%">Farmer
		Mobile APP</a> <a href="#" class="header-logo disabled"></a> <a href="#"
		class="header-icon back-button header-icon-1 font-10 no-border"><i
		class="fa fa-chevron-left"></i></a> <a
		href="${pageContext.request.contextPath}/member/news/PF2"
		class="header-icon header-icon-2 no-border font-14"
		data-deploy-menu="menu-4"><i class="fa fa-home"></i></a> <a href="#"
		class="header-icon header-icon-4 hamburger-animated"
		data-deploy-menu="menu-1"><em class="hm1"></em><em class="hm2"></em><em
		class="hm3"></em></a>

</div>
<br>

<!-- style="background-image: linear-gradient(to right, black, rgb(170, 8, 70)); transition: all 350ms ease 0s;" -->