<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<script type="text/javascript" src="${pageContext.request.contextPath}/contents/scripts/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/contents/scripts/custom.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/contents/scripts/plugins.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/contents/scripts/charts.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>