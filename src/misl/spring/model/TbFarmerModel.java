package misl.spring.model;

public class TbFarmerModel {
	private int farmerId;
	private String farmerName;
	private String farmerSurname;
	private String farmerAddr;
	private String farmerTel;
	private TbMemberModel memberId;
	private String timeReg;
	
	public int getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}
	public String getFarmerName() {
		return farmerName;
	}
	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}
	public String getFarmerSurname() {
		return farmerSurname;
	}
	public void setFarmerSurname(String farmerSurname) {
		this.farmerSurname = farmerSurname;
	}
	public String getFarmerAddr() {
		return farmerAddr;
	}
	public void setFarmerAddr(String farmerAddr) {
		this.farmerAddr = farmerAddr;
	}
	public String getFarmerTel() {
		return farmerTel;
	}
	public void setFarmerTel(String farmerTel) {
		this.farmerTel = farmerTel;
	}
	public TbMemberModel getMemberId() {
		return memberId;
	}
	public void setMemberId(TbMemberModel memberId) {
		this.memberId = memberId;
	}
	public String getTimeReg() {
		return timeReg;
	}
	public void setTimeReg(String timeReg) {
		this.timeReg = timeReg;
	}
	
}
