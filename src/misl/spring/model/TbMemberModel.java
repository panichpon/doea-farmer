package misl.spring.model;

public class TbMemberModel {
	private int memberId;
	private String memberName;
	private String memberSurname;
	private String memberEmail;
	private String memberPass;
	private String memberAddr;
	private String memberTel;
	private String timeReg;
	
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberSurname() {
		return memberSurname;
	}
	public void setMemberSurname(String memberSurname) {
		this.memberSurname = memberSurname;
	}
	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public String getMemberPass() {
		return memberPass;
	}
	public void setMemberPass(String memberPass) {
		this.memberPass = memberPass;
	}
	public String getMemberAddr() {
		return memberAddr;
	}
	public void setMemberAddr(String memberAddr) {
		this.memberAddr = memberAddr;
	}
	public String getMemberTel() {
		return memberTel;
	}
	public void setMemberTel(String memberTel) {
		this.memberTel = memberTel;
	}
	public String getTimeReg() {
		return timeReg;
	}
	public void setTimeReg(String timeReg) {
		this.timeReg = timeReg;
	}
	
	
}
