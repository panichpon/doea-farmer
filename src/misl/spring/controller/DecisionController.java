package misl.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/member/decision")
public class DecisionController {
	@RequestMapping(value = "/PF35", method = RequestMethod.GET)
	public ModelAndView home(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF35");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF351", method = RequestMethod.GET)
	public ModelAndView PF351(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF351");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF3511", method = RequestMethod.GET)
	public ModelAndView PF3511(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF3511");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	
	@RequestMapping(value = "/PF352", method = RequestMethod.GET)
	public ModelAndView PF352(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF352");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF3521", method = RequestMethod.GET)
	public ModelAndView PF3521(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF3521");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF35211", method = RequestMethod.GET)
	public ModelAndView PF35211(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF35211");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF352111", method = RequestMethod.GET)
	public ModelAndView PF352111(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF352111");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF3522", method = RequestMethod.GET)
	public ModelAndView PF3522(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF3522");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF35221", method = RequestMethod.GET)
	public ModelAndView PF35221(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF35221");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF353", method = RequestMethod.GET)
	public ModelAndView PF353(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF353");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF3531", method = RequestMethod.GET)
	public ModelAndView PF3531(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF3531");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF354", method = RequestMethod.GET)
	public ModelAndView PF354(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF354");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	@RequestMapping(value = "/PF355", method = RequestMethod.GET)
	public ModelAndView PF355(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/decision/PF355");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
}
