package misl.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/member/news")

public class NewsController {
	
	@RequestMapping(value = "/PF1", method = RequestMethod.GET)
	public ModelAndView PF1(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/news/PF1");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model; 
	}
	@RequestMapping(value = "/PF2", method = RequestMethod.GET)
	public ModelAndView PF2(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/news/PF2");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model; 
	}
	@RequestMapping(value = "/PF2_1", method = RequestMethod.GET)
	public ModelAndView PF2_1(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/news/PF2_1");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model; 
	}
	@RequestMapping(value = "/PF21", method = RequestMethod.GET)
	public ModelAndView PF21(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/news/PF21");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model; 
	}
	@RequestMapping(value = "/PF31", method = RequestMethod.GET)
	public ModelAndView PF31(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/news/PF31");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model; 
	}
}
