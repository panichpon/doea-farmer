package misl.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/member/questionnaire/")
public class QuestionnaireController {
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView("member/questionnaire/home");
		try { 
			return model; 
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}	
	@RequestMapping(value = "/Answer", method = RequestMethod.GET)
	public ModelAndView Answer(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		ModelAndView model = new ModelAndView("member/questionnaire/Answer");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/Answer2", method = RequestMethod.GET)
	public ModelAndView Answer2(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		ModelAndView model = new ModelAndView("member/questionnaire/Answer2");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}

}
