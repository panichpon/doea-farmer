package misl.spring.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/member/socaillistening/")
public class SocailController {
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpSession session, HttpServletRequest resquest, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try { 
			return model; 
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/SocailListening", method = RequestMethod.GET)
	public ModelAndView home2(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		ModelAndView model = new ModelAndView("/member/socaillistening/home2");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public ModelAndView home3(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		ModelAndView model = new ModelAndView("/member/socaillistening/home3");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/reportStatus", method = RequestMethod.GET)
	public ModelAndView home4(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		ModelAndView model = new ModelAndView("member/socaillistening/home4");
		try {
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/graphvalue", method = RequestMethod.POST)
	public @ResponseBody Object graphvalue(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ArrayList<Integer> gvalue = new ArrayList<>();
		gvalue.add(20);
		gvalue.add(50);
		gvalue.add(30);
		
		return gvalue;
		
	}
}
