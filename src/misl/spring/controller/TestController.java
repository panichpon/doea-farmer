package misl.spring.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import misl.dao.TbFarmerDAO;
import misl.dao.TbMemberDAO;
import misl.db.Database;
import misl.spring.model.CowModel;
import misl.spring.model.JsonResponseModel;
import misl.spring.model.MemberModel;
import misl.spring.model.TbFarmerModel;
import misl.spring.model.TbMemberModel;

@Controller
@RequestMapping("/test")
public class TestController {
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try {
			//Database query here
			MemberModel memberModel = new MemberModel();
			memberModel.setMemberId(1);
			memberModel.setMemberName("PoN");
			memberModel.setMemberSurname("PoN[L]");
			memberModel.setMemberAddr("224 หมู่ 20");
			memberModel.setMemberTel("0830364720");
			
			ArrayList<CowModel> cowList = new ArrayList<CowModel>();
			
			for (int i = 0; i < 10; i++) {
				CowModel cowModel = new CowModel();
				cowModel.setCowId(i);
				cowModel.setCowName("Cow"+i);
				cowModel.setCowBirth("10-03-2019");
				cowModel.setCowImg("cowDemo.jpg");
				cowModel.setCowSex("F");
				cowModel.setZyanId("1237049812734981273487213847");
				cowModel.setMember(memberModel);
				
				cowList.add(cowModel);
			}
			// End Database
			
			request.setAttribute("cowList", cowList);
			
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profile(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try {
			//Database query here
			MemberModel memberModel = new MemberModel();
			memberModel.setMemberId(1);
			memberModel.setMemberName("PoN");
			memberModel.setMemberSurname("PoN[L]");
			memberModel.setMemberAddr("224 หมู่ 20");
			memberModel.setMemberTel("0830364720");
			
			// End Database
			
			request.setAttribute("memberModel", memberModel);
			
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try {
			
			
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/add_register", method = RequestMethod.POST)
	@ResponseBody
	public JsonResponseModel add_register(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		JsonResponseModel res = new JsonResponseModel();
		try {
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String addr = request.getParameter("addr");
			String tel = request.getParameter("tel");
			
			TbMemberModel memberModel = new TbMemberModel();
			memberModel.setMemberName(name);
			memberModel.setMemberSurname(surname);
			memberModel.setMemberEmail(email);
			memberModel.setMemberPass(password);
			memberModel.setMemberAddr(addr);
			memberModel.setMemberTel(tel);
			
			
			System.out.println(name + ", " + surname + ", " + email + ", " + password + ", " + addr + ", " + tel);
			
			if (email.contains("@")) {
				//member insert
				Database db = new Database();
				TbMemberDAO memberDAO = new TbMemberDAO(db);
				int memberLastId = memberDAO.Add(memberModel);
				db.close();
				
				//farmer insert
				TbFarmerModel farmerModel = new TbFarmerModel();
				farmerModel.setFarmerName("");
				farmerModel.setFarmerSurname("");
				farmerModel.setFarmerAddr("");
				farmerModel.setFarmerTel("");
				memberModel.setMemberId(memberLastId);
				farmerModel.setMemberId(memberModel);
				
				db = new Database();
				TbFarmerDAO farmerDAO = new TbFarmerDAO(db);
				int farmerLastId = farmerDAO.Add(farmerModel);
				db.close();
				
				System.out.println("memberId: " + memberLastId);
				System.out.println("farmerId: " + farmerLastId);
				
				res.setResult("success");
				res.setStatus("200");
				res.setLabel("resgister member ID " +memberLastId+ " successed!");
			} else {
				res.setResult("fail");
				res.setStatus("200");
				res.setLabel("resgister member fail!");
			}
			return res;
		} catch (Exception e) {
			res.setResult("fail");
			res.setStatus("500");
			res.setLabel("internal server error!");
		}
		return res;
	}
	
	@RequestMapping(value = "/member_view", method = RequestMethod.GET)
	public ModelAndView member_view(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try {
			Database db = new Database();
			TbMemberDAO memberDAO = new TbMemberDAO(db);
			ArrayList<TbMemberModel> memberList = memberDAO.FindAll();
			System.out.println(memberList.size());
			db.close();
			
			request.setAttribute("memberList", memberList);
			
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/member_delete", method = RequestMethod.GET)
	@ResponseBody
	public JsonResponseModel member_delete(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		JsonResponseModel res = new JsonResponseModel();
		try {
			int member_id = Integer.parseInt(request.getParameter("member_id").toString());
			
			Database db = new Database();
			TbMemberDAO memberDAO = new TbMemberDAO(db);
			TbMemberModel memberModel = new TbMemberModel();
			memberModel.setMemberId(member_id);
			int returnCode = memberDAO.Delete(memberModel);
			db.close();
			
			System.out.println(returnCode);
			res.setResult("success");
			res.setStatus("200");
			res.setLabel("delete member successed!");
			
			return res;
		} catch (Exception e) {
			res.setResult("fail");
			res.setStatus("500");
			res.setLabel("internal server error!");
		}
		return res;
	}
	
	@RequestMapping(value = "/member_edit", method = RequestMethod.GET)
	public ModelAndView member_edit(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");
		
		ModelAndView model = new ModelAndView();
		try {
			
			int member_id = Integer.parseInt(request.getParameter("member_id").toString());
			
			Database db = new Database();
			TbMemberDAO memberDAO = new TbMemberDAO(db);
			TbMemberModel memberModel = memberDAO.FindByID(member_id);
			db.close();
			
			request.setAttribute("memberModel", memberModel);
			
			return model;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
	}
	
	@RequestMapping(value = "/member_edit_confirm", method = RequestMethod.POST)
	public ModelAndView member_edit_confirm(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("text/html;charset=UTF-8");

		try {
			int member_id = Integer.parseInt(request.getParameter("member_id").toString());
			String name = request.getParameter("name");
			String surname = request.getParameter("surname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String addr = request.getParameter("addr");
			String tel = request.getParameter("tel");
			
			TbMemberModel memberModel = new TbMemberModel();
			memberModel.setMemberName(name);
			memberModel.setMemberSurname(surname);
			memberModel.setMemberEmail(email);
			memberModel.setMemberPass(password);
			memberModel.setMemberAddr(addr);
			memberModel.setMemberTel(tel);
			memberModel.setMemberId(member_id);
			
			Database db = new Database();
			TbMemberDAO memberDAO = new TbMemberDAO(db);
			int returnCode = memberDAO.Update(memberModel);
			db.close();
			
			System.out.println(returnCode);
			
			return new ModelAndView("redirect:/test/member_view");
		} catch (Exception e) {
			;
		}
		return new ModelAndView("redirect:/test/member_view");
	}
}
