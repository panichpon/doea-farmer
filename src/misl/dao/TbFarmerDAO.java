package misl.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import misl.dao.impl.DAO;
import misl.db.Database;
import misl.spring.model.TbFarmerModel;
import misl.spring.model.TbMemberModel;

public class TbFarmerDAO implements DAO<TbFarmerModel> {

	Database db;
	public TbFarmerDAO(Database db) {
		this.db = db;
	}
	
	@Override
	public int Add(TbFarmerModel bean) {
		String sql = "INSERT INTO tb_farmer (farmer_name, farmer_surname, farmer_addr, farmer_tel, member_id) "
				+ "VALUES ('"+bean.getFarmerName()+"', "
				+ "'"+bean.getFarmerSurname()+"', "
				+ "'"+bean.getFarmerAddr()+"', "
				+ "'"+bean.getFarmerTel()+"', "
				+ "'"+bean.getMemberId().getMemberId()+"')";
		return db.add(sql, new String[] {"farmer_id"});
	}

	@Override
	public int Delete(TbFarmerModel bean) {
		String sql = "DELETE FROM tb_farmer WHERE farmer_id = " + bean.getFarmerId();
		return db.remove(sql);
	}

	@Override
	public int Update(TbFarmerModel bean) {
		String sql = "UPDATE tb_farmer SET "
				+ "farmer_name = '"+bean.getFarmerName()+"', "
				+ "farmer_surname = '"+bean.getFarmerSurname()+"', "
				+ "farmer_addr = '"+bean.getFarmerAddr()+"', "
				+ "farmer_tel = '"+bean.getFarmerTel()+"', "
				+ "member_id = '"+bean.getMemberId().getMemberId()+"' "
				+ "WHERE farmer_id = " + bean.getFarmerId();
		return db.update(sql);
	}

	@Override
	public ArrayList<TbFarmerModel> FindAll() {
		String sql = "SELECT * FROM tb_member ORDER BY member_id ASC";
		ArrayList<HashMap<String, Object>> queryList = db.queryList(sql);
		ArrayList<TbFarmerModel> farmerList = new ArrayList<TbFarmerModel>();
		for (Iterator<HashMap<String, Object>> iterator = queryList.iterator(); iterator.hasNext();) {
			HashMap<String, Object> next = iterator.next();
			TbFarmerModel farmer = MappingBeans(next);
			farmerList.add(farmer);
		}
		return farmerList;
	}

	@Override
	public TbFarmerModel FindByID(TbFarmerModel bean) {
		String sql = "SELECT * FROM tb_farmer WHERE farmer_id = " + bean.getFarmerId();
		HashMap<String, Object> querySingle = db.querySingle(sql);
		TbFarmerModel farmerModel = MappingBeans(querySingle);
		return farmerModel;
	}

	@Override
	public TbFarmerModel FindByID(int id) {
		String sql = "SELECT * FROM tb_farmer WHERE farmer_id = " + id;
		HashMap<String, Object> querySingle = db.querySingle(sql);
		TbFarmerModel farmerModel = MappingBeans(querySingle);
		return farmerModel;
	}

	@Override
	public TbFarmerModel FindByUUID(String uuid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TbFarmerModel MappingBeans(HashMap<String, Object> map) {
		
		Database db = new Database();
		TbMemberDAO memberDAO = new TbMemberDAO(db);
		TbMemberModel memberModel = memberDAO.FindByID(Integer.parseInt(map.get("member_id").toString()));
		db.close();
		
		TbFarmerModel farmerModel = new TbFarmerModel();
		
		farmerModel.setFarmerId(Integer.parseInt(map.get("farmer_id").toString()));
		farmerModel.setFarmerName(map.get("farmer_name").toString());
		farmerModel.setFarmerSurname(map.get("farmer_surname").toString());
		farmerModel.setFarmerAddr(map.get("farmer_addr").toString());
		farmerModel.setMemberId(memberModel);
		
		return farmerModel;
	}

}
