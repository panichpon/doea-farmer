package misl.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import misl.dao.impl.DAO;
import misl.db.Database;
import misl.spring.model.TbMemberModel;

public class TbMemberDAO implements DAO<TbMemberModel> {
	
	Database db;
	
	public TbMemberDAO(Database db) {
		this.db = db;
	}

	@Override
	public int Add(TbMemberModel bean) {
		String sql = "INSERT INTO tb_member (member_name, member_surname, member_email, member_pass, member_addr, member_tel) "
				+ "VALUES ('"+bean.getMemberName()+"', "
				+ "'"+bean.getMemberSurname()+"', "
				+ "'"+bean.getMemberEmail()+"', "
				+ "'"+bean.getMemberPass()+"', "
				+ "'"+bean.getMemberAddr()+"', "
				+ "'"+bean.getMemberTel()+"')";
		return db.add(sql, new String[] {"member_id"});
	}

	@Override
	public int Delete(TbMemberModel bean) {
		String sql = "DELETE FROM tb_member WHERE member_id = " + bean.getMemberId();
		return db.remove(sql);
	}

	@Override
	public int Update(TbMemberModel bean) {
		String sql = "UPDATE tb_member SET "
				+ "member_name = '"+bean.getMemberName()+"', "
				+ "member_surname = '"+bean.getMemberSurname()+"', "
				+ "member_email = '"+bean.getMemberEmail()+"', "
				+ "member_pass = '"+bean.getMemberPass()+"', "
				+ "member_addr = '"+bean.getMemberAddr()+"', "
				+ "member_tel = '"+bean.getMemberTel()+"' "
				+ "WHERE member_id = " + bean.getMemberId();
		return db.update(sql);
	}

	@Override
	public ArrayList<TbMemberModel> FindAll() {
		String sql = "SELECT * FROM tb_member ORDER BY member_id ASC";
		ArrayList<HashMap<String, Object>> queryList = db.queryList(sql);
		ArrayList<TbMemberModel> memberList = new ArrayList<TbMemberModel>();
		System.out.println("OK1");
		for (Iterator<HashMap<String, Object>> iterator = queryList.iterator(); iterator.hasNext();) {
			HashMap<String, Object> next = iterator.next();
			TbMemberModel member = MappingBeans(next);
			System.out.println("OK2");
			memberList.add(member);
		}
		return memberList;
	}

	@Override
	public TbMemberModel FindByID(TbMemberModel bean) {
		String sql = "SELECT * FROM tb_member WHERE member_id = " + bean.getMemberId();
		HashMap<String, Object> querySingle = db.querySingle(sql);
		TbMemberModel memberModel = MappingBeans(querySingle);
		return memberModel;
	}

	@Override
	public TbMemberModel FindByID(int id) {
		String sql = "SELECT * FROM tb_member WHERE member_id = " + id;
		HashMap<String, Object> querySingle = db.querySingle(sql);
		TbMemberModel memberModel = MappingBeans(querySingle);
		return memberModel;
	}

	@Override
	public TbMemberModel FindByUUID(String uuid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TbMemberModel MappingBeans(HashMap<String, Object> map) {
		TbMemberModel memberModel = new TbMemberModel();
		
		memberModel.setMemberId(Integer.parseInt(map.get("member_id").toString()));
		memberModel.setMemberName(map.get("member_name").toString());
		memberModel.setMemberSurname(map.get("member_surname").toString());
		memberModel.setMemberEmail(map.get("member_email").toString());
		memberModel.setMemberPass(map.get("member_pass").toString());
		memberModel.setMemberAddr(map.get("member_addr").toString());
		memberModel.setMemberTel(map.get("member_tel").toString());
		memberModel.setTimeReg(map.get("time_reg").toString());
		
		return memberModel;
	}

}
